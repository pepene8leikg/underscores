<?php
/**
* The template for displaying archive pages
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package _s
*/

get_header(); ?>

    <div id="primary" class="content-area main-posts col-md-posts col-sm-12 col-xs-12">
        <header class="category-page-header col-md-12">
			<?php
			the_archive_title( '<p class="page-title">', '</p>' );
			the_archive_description( '<div class="archive-description">', '</div>' );
			?>
        </header><!-- .page-header -->
        <main id="main" class="posts-section col-2-grid site-main">

			<?php
			if ( have_posts() ) : ?>
				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'post-types/content', get_post_format() );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'post-types/content', 'none' );

			endif; ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
