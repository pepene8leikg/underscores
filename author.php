<?php
/**
* The template for displaying author pages.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package _s
*/

get_header(); ?>

<div id="primary" class="content-area main-posts col-md-posts col-sm-12 col-xs-12">
	<header class="author-page-header col-md-12">
	<?php
		/* translators: %s: search query. */
		printf( esc_html__( 'Posts by: %s', '_s' ), '<span class="name">' . get_the_author() . '</span>' );
		echo '<span class="avatar">' .  get_avatar( get_the_author_meta( 'ID' ) , 48 ) . '</span>';
	?>
	</header><!-- .page-header -->
	<main id="main" class="posts-section col-2-grid site-main">
		<?php
		if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'post-types/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'post-types/content', 'none' );

		endif; ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
