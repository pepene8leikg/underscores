I went from about 1.2sec in render time to about 0.2ms in render time, much more could be achieved if I somehow find a way to cache
certain "html generated" parts, such as the menu.

I'm looking at "low update" items, such as the menu, or really anything, to "shard", as in, to make them cached independently, where as
the posts, widgets or really anything else is "live", these are not.

I'm talking about a selective caching, for when there's no caching enabled, making the theme extremely fast without losing anything.


Addtionally, I believe that what could be the difference between great caching and just simply-amazing caching is a set of "affect actions", as in,
when the administrator does an action, say "update_menu", I can re-create the cache, as so, for all purposes, nor the administrator, nor the users
see the effect of the cache "delayed update", they just see a fast-loading menu.

I'm hoping to achieve this as soon as possible.