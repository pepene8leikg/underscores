<?php
/**
* Template for how to display a single post, regardless of type. The "post visual layout".
*
*
* @package _s
*/
?>


<article id="single-post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>
	<?php
	echo
		(new Social_Space\_s_Post_Share // Initialize the class, has a constructor with variables.
			(
				// The constructor variables.
				array( 'facebook', 'twitter', 'gplus', 'pinterest' ), 
				$post, 
				$style = 'share-style-2' 
			) 
		)
		// The method that we use after the class has been initialized.
		->generate_share_links();
	?>
	<header class="single-post-header">
		<div class="single-post-meta">
			<?php echo Helpers\_s_show_post_info( array( 'author','time') ); ?>
		</div><!-- .post-meta -->

		<?php
		the_title( '<h1 class="single-post-title">','</h1>' );
		if ( has_category() ) : ?>
			<?php

			$categories = (array) wp_get_post_terms( get_the_ID(), 'category' );

			if ( !is_wp_error( $categories ) && !empty( $categories) ) { ?>
				<div class="single-post-secondary-meta">
					<span class="single-post-category-span">posted in
						<a class="single-post-category" href="<?php echo get_term_link( $categories[0] )?>"><?php echo $categories[0] -> name ?>
						</a>
					</span>
				</div><!-- .post-meta-2 -->
			<?php } ?>
		<?php endif; ?>
	</header><!-- .post-header -->

	<?php if ( has_post_thumbnail() ) : ?>
		<?php if ( !has_post_format( $format = array( 'video', 'audio') ) ) : ?>
			<div class="single-post-thumbnail">
				<a class="single-post-thumbnail-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail(); ?>
				</a>
			</div><!-- .post-thumbnail -->
		<?php endif; ?>
	<?php endif; ?>

	<div class="single-post-content">
		<?php
			//echo wp_trim_words( get_the_content(), 40, '...' );
			the_content();
		?>
	</div><!-- .post-content -->
</article><!-- #post-<?php the_ID(); ?> -->
<?php 
get_template_part('template-parts/individual_post-related-posts');
?>