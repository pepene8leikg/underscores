<?php

namespace Emails;

class Email_Hooks {

	public static function init() {
		require_once 'EmailTemplates/template-2.php';
	}

	public function getSubject($data) {
		return __( $data . ', your listing has been approved!', '_s' );
	}

	public function getBody($data) {
		return __( 'Your listing' . $data[0] . 'has been added to our directory. See more details...' . $data[1], '_s');
	}

	public function sendEmail( $users_to_send_to, $email_parts ) {

		foreach( $users_to_send_to as $user ) {
			wp_mail( $user, $email_parts['subject'], $email_parts['description'] );
		}
	}
}

Email_Hooks::init();