<?php

namespace Emails;

class Template extends Email_Hooks {

	private $data;

	function __construct( $outer_data ) {
		$this->data = $outer_data;
	}

	public function get_subject() {
		return __( $this->data[0] . ', your listing has been approved!', '_s' );
	}

	public function get_body() {
		return __( 'Your listing' . $this->data[1] . 'has been added to our directory. See more details...' . $this->data[2], '_s');
	}

	private function buildTemplate() {
		$subject		= $this->get_subject();
		$description	= $this->get_body();

		return [
			'subject' 		=> $subject,
			'description' 	=> $description,
		];
	}

	public static function hook() {

	    add_action( 'init', function() {
	        $email = new self;

	        $email->sendEmail( array('test@gmail.com'),
	              array( $info['subject'], $info['description'] )
	          );
	    } );
	}
}

Template::hook();