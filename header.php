<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package _s
*/

//Setup_Screen\Load_Setup_Screen::instance()->render_screen();
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<script type="text/javascript">
    	var ABSPATH = "<?php bloginfo('template_url'); ?>";
	</script>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<?php
	Contact_Tooltip::show_tooltip();
	// Extremely problematic. Action cannot be unhooked this way.
	Helpers\get_header_style();
	?>
	<div id="content" class="site-content main-layout container main-content">
