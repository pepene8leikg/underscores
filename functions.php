<?php
/**
* _s functions and definitions
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*
* @package _s
*/

if( !defined('THEME_DIR') ) {
	define ( 'THEME_DIR', get_template_directory() );
}

/**
* Auto-load files.
*/
require_once THEME_DIR . '/inc/autoload.php';


if( !defined('THEME_DIR') ) {
    define ( 'THEME_DIR', get_template_directory() );
}


require_once THEME_DIR . '/inc/autoloader.php';
new SproutLoader(__DIR__);

new Sprout\Tooltips\Tooltip([]);



if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
* Load up the theme's first-screen setup. The juicy stuff!
*/
require get_template_directory() . '/inc/Setup/setup-page.php';
require get_template_directory() . '/inc/Setup/setup-parser.php';