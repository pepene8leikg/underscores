<header id="masthead" class="header header-2">
		<p> Header 2 </p>
		<div class="header-focus">
			<div class="container">
				<div class="top-part col-md-12">
					<div class="informatory-header-text col-md-4">
						<div class="text"><?php echo get_theme_mod('header_custom_text_top') ?><p class="big-text"><?php echo get_theme_mod('header_custom_text_bottom') ?></p></div>
					</div>
					<div class="header-logo col-md-4">
						<?php
						the_custom_logo();
						?>
					</div>
					<div class="header-social-icons-list col-md-4">
						<ul class="social-icons">
							<li class="social-icon"></li>
						</ul>
					</div>
				</div>
				<div class="bottom-part col-md-12">
					<nav id="site-navigation" class="header-menu col-md-12">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
								'menu_class'	 => 'menu szWeo1-unique-menu'
							) );
						?>
					</nav><!-- #site-navigation -->
					<div class="search-top">
				        <div class="search-icon">
                        	<div class="search-btn"><img src="<?php echo get_template_directory_uri(); ?>/svg/search.svg"></div>
                    	</div>
	                    <form class="search-top-form" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	                        <input type="text" placeholder="Type in and press enter..." name="s" id="search_term" />
	                    </form>
					</div>
				 	<div class="hamburger-wrapper">
	                    <div class="menu-toggle">
	                        <div class="hamburger"><span></span><span></span><span></span></div>
	                        <div class="cross"><span></span><span></span></div>
	                    </div>
	                    <ul class="hamburger-dropdown">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'primary-menu-hamburger',
									'menu_class'	 => 'menu szWeo2-unique-menu'
								) );
							?>
	                    </ul>
                	</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->