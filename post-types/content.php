<?php
/**
* Template part for displaying posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package _s
*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post grid-item post-margin' ); ?>>
	<?php
	echo
		(new Social_Space\_s_Post_Share // Initialize the class, has a constructor with variables.
			(
				// The constructor variables.
				array( 'facebook', 'twitter', 'gplus', 'pinterest' ), 
				$post, 
				$style = 'share-style-1' 
			) 
		)
		// The method that we use after the class has been initialized.
		->generate_share_links();
	?>
	<header class="post-header">
		<div class="post-meta">
			<?php echo Helpers\_s_show_post_info( array( 'author','time') ); ?>
		</div><!-- .post-meta -->

		<?php
		echo Helpers\_s_trim_post_title(24, '...');
		if ( has_category() ) : ?>
			<?php

			$categories = (array) wp_get_post_terms( get_the_ID(), 'category' );

			if ( !is_wp_error( $categories ) && !empty( $categories) ) { ?>
				<div class="post-secondary-meta">
					<span class="post-category-span">posted in
						<a class="post-category" href="<?php echo get_term_link( $categories[0] )?>"><?php echo $categories[0] -> name ?>
						</a>
					</span>
				</div><!-- .post-meta-2 -->
			<?php } ?>
		<?php endif; ?>
	</header><!-- .post-header -->
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="post-thumbnail hover-thumb">
			<a class="post-thumbnail-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php the_post_thumbnail(); ?>
			</a>
		</div><!-- .post-thimbnail -->
	<?php 
	endif; ?>

	<div class="post-content">
		<?php
			//echo wp_trim_words( get_the_content(), 40, '...' );
			the_excerpt();
		?>
	</div><!-- .post-content -->

	<div class="post-read-more">
		<button type="button" class="button-continue-reading">
			<a href="<?php esc_url( the_permalink() ); ?>" target="_blank">Read More</a>
		</button>
	</div><!-- .post-read-more -->
</article><!-- #post-<?php the_ID(); ?> -->
