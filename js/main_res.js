(function($) {
    function sliceMenu(desired_len) {
        /*
        Synopsis: Cuts the main menu and puts it in a "+" dropdown based on desired length. Usage
        is clear when the site has a very long list of items in the menu, instead of breaking down
        the layout and creating issues, let's put the "remains" in a dropdown!

        param[menu_selector]:           The selector for the main menu, has to be an UL.        
        param[desired_len]:             The desired length after which we want to cut. Eg: Set to 5, the script will only
                                        display the first 5 items, putting everything after it in the dropdown.
        */
        var menu_len = document.querySelectorAll('#primary-menu > li').length;

        if (menu_len > desired_len) {

            var append_menu_el = document.createElement('div');
            append_menu_el.setAttribute("id", "js-cover-menu" );
            append_menu_el.innerHTML = ('<img class="plus" src="$PATH/svg/plus_menu.svg" alt="more-menu"></div>').replace('$PATH', ABSPATH);
            document.getElementById('primary-menu').parentNode.appendChild(append_menu_el);

            let diff = menu_len - desired_len;
            let sliced_lis = [...(document.querySelectorAll('#primary-menu > li'))].slice(-diff);


            var js_menu_el = document.createElement('ul');
            js_menu_el.setAttribute('id', 'js-cover-menu-dropdown');
            document.getElementById('js-cover-menu').appendChild(js_menu_el);

            js_cover_menu_el = document.getElementById('js-cover-menu-dropdown');

            for(var i = 0, length = sliced_lis.length; i < length; i++) {
                js_cover_menu_el.appendChild(sliced_lis[i]);
            }
            document.querySelectorAll('#js-cover-menu-dropdown > li > ul').forEach(node => node.remove());
        } else {
            return;
        }
    }

    function enableHamburgerAnimation(which_one) {
        /*
         Synopsis: Enables the Hamburger Menu Animation & its extensions.

         param[which_one]:              Denotes which animation is to be loaded. We might have more! :)
         */
        if(which_one === "1") {
            try {
                $('.menu-toggle').click(function(){
                    $(this).toggleClass('open');
                    $('.hamburger-dropdown').toggleClass('active');
                });
                console.log("Loaded Hamburger-1 Animation!");
            } catch(err) {
                console.log(err);
            }
        } else if(which_one === "2") {
            console.log("Loaded Hamburger-2 Animation!");
        }
    }

    window.onload = function() {

        $(document).ready(function($) {
            try {
                sliceMenu(3);
                enableHamburgerAnimation("1");
                $(".single-post-content").fitVids();
                console.log(performance.now());
            } catch(err) {
                console.log(err);
            }
        });
    };
})(jQuery);