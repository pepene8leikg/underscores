(function($) {
	$("#page").append("<input type='button' value='' class='show-tooltips'>");
	
	var tooltip_selector = $('.show_tooltips');

	if(passed_wp_variables.show_state == 'show') {
		$('.show-tooltips').val('Disable Helper Tooltips');
	} else {
		$('.show-tooltips').val('Show Helper Tooltips');
	}

	$('.show-tooltips').on("click", function(e) {
		e.preventDefault();
		if(passed_wp_variables.show_state == 'show') {
			setTooltips('hide');
			console.log("Tooltips are shown!");
		} else {
			setTooltips('show');
			console.log("Tooltips not shown!")
		}
	});

})(jQuery);


function setTooltips(show_state) {
	
	var ajaxurl =  passed_wp_variables.ajaxurl;
	var ajaxsecurity = passed_wp_variables.ajax_nonce;

	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		dataType: 'json',
		data: {
			action: 'parse_tooltips_data',
			security: ajaxsecurity,
			tooltip_data: show_state
		},

		success: function(response) {
			window.location.href = passed_wp_variables.redirect_url;
			console.log(response);
		},

		error: function(error) {
			console.log(error);
		}
	});
}