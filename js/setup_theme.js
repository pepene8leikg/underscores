(function($) {

	/* Screens Controls Object */
	var screen_controls = {
		screen_numbers: 2
	};


	/*Cacheables */
	var SelectorCache = {
		welcome_screen: $('.welcome_screen')
	};

	SelectorCache.welcome_screen.data("form-data", {});

	$('.go').on("click", function(e) {
		e.preventDefault();
		animateStep1(function() {
	      	genesis(
	      		'<div id="screen-1" style="z-index:2;"></div>',
				'#screen-1', 
				'/js/setup_theme_templates/template_screen_html-1.html',
				'.top-part'
			);		
		});
		$(this).unbind();
	});

	for(var i = 1; i <= screen_controls.screen_numbers; i++) {
		SelectorCache.welcome_screen.on("submit", "#myForm-" + i, function(event) {
			event.preventDefault();

			var index = parseInt($(this).attr("id").split("-")[1]) + 1;

			var __construct_getDataForm = window["getDataForm" + (index - 1)];
			__construct_getDataForm();

			genesis(
				'<div id="screen-' + index + '" style="z-index:2;"></div>',
				'#screen-' + index, 
				'/js/setup_theme_templates/template_screen_html-' + index + '.html',
				'.top-part'
			);

			var __construct_animateStep = window["animateStep" + index];
			__construct_animateStep( function(id) {
				$("#screen-" + id).remove();
			});
		});
	}

	SelectorCache.welcome_screen.on("submit", "#myForm-" + (screen_controls.screen_numbers + 1), function(event) {
		event.preventDefault();
		getDataForm_final();
	});

})(jQuery);

/* Main functions */
function genesis(screen_name, screen_selector, screen_contents, the_old) {
	let handler = '.welcome_screen';
	jQuery(handler).prepend(screen_name);
	jQuery(screen_selector).load(passed_wp_variables.ABSPATH + screen_contents);
	jQuery(the_old).remove();
}

/* Get DataForms & Animations for each screen */

/* screen1: */
function getDataForm1() {
	var formData = jQuery('.welcome_screen').data("form-data");

	var field = jQuery("input[name='name']");
	formData[field.attr("name")] = field.val();
}

function animateStep1(callback) {
	jQuery('.welcome_screen .top-part').animate({
		opacity: 0,
	}, {
		step: function() {
			jQuery(this).css({'transform':'translate3d(0,-50px,0)'});
		},
		done: callback
	});

	jQuery('.welcome_screen .go').animate({
		opacity: 1,
	}, {
		step: function() {
			jQuery(this).css({'transform':'scale(5)', 'margin-top':'0','z-index':'0', 'background-color':'#41ff89'});
			jQuery('.welcome_screen .go_bg').css({'opacity':'1','background-image':'url('+ passed_wp_variables.ABSPATH + '/res/setup_pattern.png' + ')'});
		},
		duration: 1500
	}, 'swing');

	jQuery('.welcome_screen .go-text').animate({
		opacity: 0
	});
}

/* screen2: */
function getDataForm2() {
	var formData = jQuery('.welcome_screen').data("form-data");

	var field = jQuery("input[name='bypass']");
	formData[field.attr("name")] = field.val();
}

function animateStep2(callback) {
	jQuery("#screen-1").animate({
		opacity: 0
	}, {
		complete: callback(1),
	});
	jQuery('.welcome_screen .go').animate({
		opacity: 1,
	}, {
		step: function() {
			jQuery(this).css({'background-color':'#fffd41'});
		},
		duration: 1500
	}, 'swing');
}

/* screen3*final: */
function animateStep3(callback) {
	jQuery("#screen-2").animate({
		opacity: 0
	}, {
		complete: callback(2),
	});
	jQuery('.welcome_screen .go').animate({
		opacity: 1,
	}, {
		step: function() {
			jQuery(this).css({'background-color':'#41e2ff'});
		},
		duration: 1500
	}, 'swing');			
}

/* screen_final: replace with getDataFormX for new screen.*/
function getDataForm_final() {
	var formData = jQuery('.welcome_screen').data("form-data");

	var field = jQuery("input[name='city']");
	formData[field.attr("name")] = field.val();

	var json_data = JSON.stringify(formData);
	
	var ajaxurl =  passed_wp_variables.ajaxurl;
	var ajaxsecurity = passed_wp_variables.ajax_nonce;

	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		dataType: 'json',
		data: {
			action: 'parse_setup_data',
			security: ajaxsecurity,
			data: json_data
		},

		success: function(response) {
			console.log(response);
		},

		error: function(error) {
			console.log(error);
		}
	});
}