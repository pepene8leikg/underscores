// Source: https://vedmant.com/using-wordpress-media-library-in-widgets-options/
jQuery(document).ready(function ($) {
 
   $(document).on("click", ".upload_image_button", function (e) {
      e.preventDefault();
      var $button = $(this);
 
 
      // Create the media frame.
      var file_frame = wp.media.frames.file_frame = wp.media({
         title: 'Select or upload image',
         library: { // remove these to show all
            type: 'image' // specific mime
         },
         button: {
            text: 'Select'
         },
         multiple: false  // Set to true to allow multiple files to be selected
      });
 
      // When an image is selected, run a callback.
      file_frame.on('select', function () {
         // We set multiple to false so only get one image from the uploader
         var attachment = file_frame.state().get('selection').first().toJSON();

         if(isURL(attachment.url)) {
            $button.siblings('input').val(attachment.url).change();
         } else {
            $button.siblings('input').val('#').change();
         }
 
      });
 
      // Finally, open the modal
      file_frame.open();
   });
});

function isURL(str) {
   var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
   '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name and extension
   '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
   '(\\:\\d+)?'+ // port
   '(\\/[-a-z\\d%@_.~+&:]*)*'+ // path
   '(\\?[;&a-z\\d%@_.,~+&:=-]*)?'+ // query string
   '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
   return pattern.test(str);
}