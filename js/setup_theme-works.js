(function($) {

	$('.welcome_screen').data("form-data", {});

	$('.go').on("click", function() {
		animateStep1(function() {
	      	genesis('<div id="screen-1" style="z-index:2;"></div>',
						'#screen-1', 
						'/js/setup_theme_templates/template_screen_html-1.html',
						'.top-part');		
		});
	});

	for (var i = 1; i <= 2; i++) {
		$('.welcome_screen').on("submit", "#myForm-" + i, function(event) {
			event.preventDefault();
			var index = parseInt($(this).attr("id").split("-")[1]) + 1;

			var functionName = window["getDataForm" + (index - 1)];
			functionName();

			genesis('<div id="screen-' + index + '" style="z-index:2;"></div>',
						'#screen-' + index, 
						'/js/setup_theme_templates/template_screen_html-' + index + '.html',
						'.top-part');

			var functionName = window["animateStep" + index];
			functionName(function(asd) {
				$("#screen-" + asd).remove();
			});
		});
	}

	$('.welcome_screen').on("submit", "#myForm-3", function(event) {
		event.preventDefault();
		console.log("here");
		getDataForm3();
		console.log("here2");

		$.ajax({
			url: "testing.php",
			data: $('.welcome_screen').data("form-data"),
			complete: function(response) {
				console.log(response);
			}
		})
	});

})(jQuery);

function getDataForm1() {
	var formData = jQuery('.welcome_screen').data("form-data");

	var field = jQuery("input[name='name']");
	formData[field.attr("name")] = field.val();
}

function getDataForm2() {
	var formData = jQuery('.welcome_screen').data("form-data");

	var field = jQuery("input[name='address']");
	formData[field.attr("name")] = field.val();
}

function getDataForm3() {
	var formData = jQuery('.welcome_screen').data("form-data");

	var field = jQuery("input[name='city']");
	formData[field.attr("name")] = field.val();

	//var field2 = jQuery("input[name='country']");
	//formData[field.attr("name")] = field2.val();
}

function animateStep1(callback) {
	jQuery('.welcome_screen .top-part').animate({
		opacity: 0,
	}, {
		step: function() {
			jQuery(this).css('transform', 'translate3d(0,-20px,0)');
		},
		duration: 2500,
		done: callback
	}, 'swing');

	jQuery('.welcome_screen .go').animate({
		opacity: 1,
	}, {
		step: function() {
			jQuery(this).css({'height':'100%', 'width':'100%', 'margin-top':'0','z-index':'0', 'background-color':'#0cff9b'});
		},
		duration: 1500
	}, 'swing');

	jQuery('.welcome_screen .go-text').animate({
		opacity: 0,
	}, {
		step: function() {
			jQuery(this).css('transition','all 1s ease-out');
		},
		duration: 1000
	});
}

function animateStep2(callback) {
	jQuery("#screen-1").animate({
		opacity: 0
	}, {
		easing: "swing",
		duration: 1000,
		complete: callback(1),
	});		
}

function animateStep3(callback) {
	jQuery("#screen-2").animate({
		opacity: 0
	}, {
		easing: "swing",
		duration: 1000,
		complete: callback(2),
	});				
}

function genesis(screen_name, screen_selector, screen_contents, the_old) {
	let handler = '.welcome_screen';
	jQuery(handler).prepend(screen_name);
	jQuery(screen_selector).load(ABSPATH + screen_contents);
	jQuery(the_old).remove();
}
