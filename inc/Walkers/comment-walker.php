<?php

class Custom_Comment_Walker extends Walker_Comment {
	var $tree_type = 'comment';

	var $db_fields = array( 
		'parent' => 'comment_parent', 
		'id' => 'comment_ID' 
	);

	function __construct() { ?>
		<ul class="comments-list col-md-12">

	<?php }

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		?>
		
		<ul class="child-comments comments-list col-md-12">

	<?php }

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		?>

		</ul>

	<?php }

	function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		$depth++;
		$GLOBALS['comment_depth'] = $depth;
		$GLOBALS['comment'] = $comment;
		?>

		<li <?php comment_class( $depth == '1' ? 'parent col-md-12' : 'child col-md-11') ?> id="comment-<?php comment_ID() ?>" itemprop="comment" itemscope itemtype="http://schema.org/Comment">
			<div class="comment-wrapper col-md-12">
				<?php echo get_avatar( $comment, 65, '[default gravatar URL]', 'Author’s gravatar' ); ?>
				<div class="info">
					<div class="comment-meta" role="complementary">
						<h2 class="comment-author">
							<a class="comment-author-link" href="<?php comment_author_url(); ?>" itemprop="author"><?php comment_author(); ?></a>
						</h2>
						<time class="comment-time" datetime="<?php comment_date('Y-m-d') ?>T<?php comment_time('H:iP') ?>" itemprop="datePublished"><?php comment_date('jS F Y') ?>, <a href="#comment-<?php comment_ID() ?>" itemprop="url"><?php comment_time() ?></a></time>
						<?php edit_comment_link('<p class="comment-meta-item">Edit this comment</p>','',''); ?>
						<?php if ($comment->comment_approved == '0') : ?>
						<p class="comment-time">Your comment is awaiting moderation.</p>
						<?php endif; ?>
					</div>
					<div class="comment-content" itemprop="text">
				        <?php 
				        comment_text(); 					
						comment_reply_link(
							array_merge( 
									$args, array(
											'add_below' => $add_below, 
											'depth' => $depth, 
											'max_depth' => $args['max_depth']
											)
										),
									$comment->comment_ID);

        				?>
					</div>
				</div>
			</div>

	<?php }

	function end_el(&$output, $comment, $depth = 0, $args = array() ) { ?>

		</li>

	<?php }

	function __destruct() { ?>

		</ul>
	
	<?php }
}