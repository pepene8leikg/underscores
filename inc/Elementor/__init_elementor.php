<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Elementor_Integration {

   public $widgets;

   public function __construct() {
      $this->widgets = [
         '3-inline-posts',
         '2-tall-posts',
         'list-post-style-1',
         'headline-with-bg-style-1',
         'image-with-title'
      ];

      add_action( 'elementor/init', array( $this, 'widgets_registered' ) );
   }

   public function widgets_registered() {
      if ( ! defined( 'ELEMENTOR_PATH' ) || ! class_exists( 'Elementor\Widget_Base' ) || ! class_exists( 'Elementor\Plugin' ) ) {
         return false;
      }

      $elementor = Elementor\Plugin::instance();


      foreach( $this->widgets as $widget ) {

         $template_file = get_template_directory() . "/inc/Elementor/Widgets/{$widget}.php";

         if ( file_exists( $template_file ) ) {
            require_once $template_file;
         }
      }
   }
}

new Elementor_Integration;