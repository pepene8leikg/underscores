<?php
?>
<style>
.list-posts-style-1 {
    display: flex;
}

.list-posts-style-1--the-posts {
    flex-wrap: wrap;
    flex-direction: column;
    list-style: none;
}

.list-posts-style-1--individual-post {
    display: flex;
    flex-direction: row;
    position: relative;
    padding: 24px 0;
    border-bottom: 1px solid rgba(0,0,0,0.1);
}

.list-posts-style-1--individual-post:last-child {
    border-bottom: 0;
}

.list-posts-style-1--individual-post .two-tall-posts-layout-1--individual-post-image {
    width: 100%;
}

.list-posts-style-1--individual-post-image img {
    display: block;
    object-fit: cover;
    height: 400px;
    width: 100%;
}

.list-posts-style-1--info {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
}

.list-posts-style-1--individual-post h3 {
    order: 2;
    font-family:"Simoncini Garamond Italic";
    font-size: 36px;
    margin-bottom: 6px;
    text-align: center;
}

.list-posts-style-1--info .post-meta-info {
    order: 1;
    margin-bottom: 12px;
}

.list-posts-style-1--individual-post .posted-on {
    text-transform: uppercase;
    letter-spacing: 2px;
    font-size: 9px;
    font-family: 'Helvetica', sans-serif;
    font-weight: 800;
}

.list-posts-style-1--excerpt {
    margin: 12px 0 24px 0;
    order: 3;
    text-align: center;
    padding: 0 8%;
}
</style>