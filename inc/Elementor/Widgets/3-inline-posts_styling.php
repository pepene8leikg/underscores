<?php
?>
<style>
.inline-posts-layout-1 {
    display: flex;
}

.inline-posts--the-posts {
    justify-content: space-between;
    display: inline-flex;
    list-style: none;
}

.inline-posts--individual-post {
    padding: 0 12px;
    display: flex;
    flex-direction: column;
    align-items: center;
}

.inline-posts--individual-post:first-child,
.inline-posts--individual-post:last-child {
    padding: 0;
}

.inline-posts--individual-post .inline-posts--individual-post-image {
    width: 100%;
    height: 240px;
    margin-bottom: 6px;
}

.inline-posts--individual-post-image img {
    display: block;
    object-fit: cover;
    height: 100%;
    width: 100%;
}

.inline-posts--individual-post h3 {
    font-family:"Simoncini Garamond Italic";
    font-size: 30px;
    margin-bottom: 6px;
    text-align: center;
}

.inline-posts--individual-post .posted-on {
    text-transform: uppercase;
    letter-spacing: 2px;
    font-size: 9px;
    font-family: 'Helvetica', sans-serif;
    font-weight: 800;
    text-align: center;
    opacity: 0.5;
}
</style>