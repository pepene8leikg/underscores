<?php
namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Simple_Image_With_Title extends Widget_Base {

	public function get_name() {
	    return 'simple-image-widget';
	}

	public function get_title() {
	    return __( 'Simple Image With Title', 'elementor' );
	}

	public function get_icon() {
	    return 'favorite';
	}

	public function get_custom_placeholder_image() {
		return get_template_directory_uri() . '/res/placeholder_image.jpg';
	}

	public function set_styling() {
		$current_file = basename(__FILE__, '.php');
		$template_style = get_template_directory() . "/inc/Elementor/Widgets/{$current_file}_styling.php";

		if( file_exists($template_style ) ) {
			require_once $template_style;
		}
	}

	protected function _register_controls() {

	    $this->start_controls_section(
	        'simple-image-section',
	        [
	            'label'         => __( 'Simple Image With Title', 'elementor' ),
	        ]
	    );

	    $this->add_control(
	    	'widget_image',
	    	[
	    		'label'			=> __( 'Choose an Image', 'elementor' ),
	    		'type'			=> Controls_Manager::MEDIA,
	    		'default'		=> [
	    			'url'		=> $this->get_custom_placeholder_image(),
	    		],
	    	]
	    );

	    $this->add_control(
			'show_box',
			[
				'label' => __( 'Show Big Box?', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => __( 'Show', 'elementor' ),
				'label_off' => __( 'Hide', 'elementor' ),
				'return_value' => 'yes',
			]
		);

		$this->add_control(
		    'big_box_scale',
		    [
		        'label' => __( 'Big Box Scale', 'elementor' ),
		        'type' => Controls_Manager::SLIDER,
		        'default' => [
		            'size' => 1,
		        ],
		        'range' => [
		            'px' => [
		                'min' => 48,
		                'max' => 120,
		                'step' => 4,
		            ],
		        ],
		        'selectors' => [
		            '{{WRAPPER}} .big-image-box-with-title .big-box' => 'padding: {{SIZE}}{{UNIT}};',
		        ],
		        'condition' => ['show_box' => 'yes'],
		    ]
		);


	    $this->add_control(
	    	'small_text',
	    	[
	    		'label'			=> __( 'Small Text', 'elementor' ),
	    		'type'			=> Controls_Manager::TEXT,
	    		'default'		=> 'Enjoy',
	    		'condition' => ['show_box' => 'yes']
	    	]
	    );

	    $this->add_control(
	    	'big_text',
	    	[
	    		'label'			=> __( 'Big Text', 'elementor' ),
	    		'type'			=> Controls_Manager::TEXT,
	    		'default'		=> 'The Fash Life of Ours',
	    		'condition' => ['show_box' => 'yes']
	    	]
	    );

	    $this->end_controls_section();

	}

	protected function render() {
		/**
		* Sets up the styling and loads it for the current widget.
		*/
		$this->set_styling();

	    $settings = $this->get_settings();
	    ?>
	    <div class="big-image-box-with-title">

	    	<?php
	    	if( 'yes' == $settings['show_box'] && isset( $settings['widget_image']['url'] ) ) { 
    		?>

	    		<div class="big-box">
	    			<h5 class="big-box--paragraph"><?php echo $settings['small_text']; ?></h5>
	    			<h3 class="bix-box--big-text"><?php echo $settings['big_text']; ?></h3>
	    		</div>

	    	<?php
	    	} 
	    	?>

	    	<img class="image" src="<?php echo $settings['widget_image']['url']; ?>">

	    </div>
	    <?php
	}

	protected function content_template() {}

	public function render_plain_content() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Simple_Image_With_Title() );