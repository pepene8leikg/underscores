<?php
namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Three_Inline_Posts extends Widget_Base {

	public function get_name() {
	    return 'three-inline-posts';
	}

	public function get_title() {
	    return __( '3 Inline Posts', 'elementor' );
	}

	public function get_icon() {
	    return 'favorite';
	}

	public function set_styling() {
		$current_file = basename(__FILE__, '.php');
		$template_style = get_template_directory() . "/inc/Elementor/Widgets/{$current_file}_styling.php";

		if( file_exists($template_style ) ) {
			require_once $template_style;
		}
	}

	protected function _register_controls() {

	    $this->start_controls_section(
	        'three-inline-posts',
	        [
	            'label'         => __( 'Three Inline Posts', 'elementor' ),
	        ]
	    );

	    $this->add_control(
	        'order',
	        [
	            'label'         => __( 'Order', 'elementor' ),
	            'type'          => Controls_Manager::SELECT,
	            'default'       => '',
	            'options'       => [
	                ''          => __( 'Default', 'elementor' ),
	                'DESC'      => __( 'DESC', 'elementor' ),
	                'ASC'       => __( 'ASC', 'elementor' ),
	            ],
	        ]
	    );

	    $this->add_control(
	        'orderby',
	        [
	            'label'         => __( 'Order By', 'elementor' ),
	            'type'          => Controls_Manager::SELECT,
	            'default'       => '',
	            'options'       => [
	                ''              => __( 'Default', 'elementor' ),
	                'date'          => __( 'Date', 'elementor' ),
	                'title'         => __( 'Title', 'elementor' ),
	                'name'          => __( 'Name', 'elementor' ),
	                'modified'      => __( 'Modified', 'elementor' ),
	                'author'        => __( 'Author', 'elementor' ),
	                'rand'          => __( 'Random', 'elementor' ),
	                'ID'            => __( 'ID', 'elementor' ),
	                'comment_count' => __( 'Comment Count', 'elementor' ),
	                'menu_order'    => __( 'Menu Order', 'elementor' ),
	            ],
	        ]
	    );

	    $this->end_controls_section();

	}
	protected function render() {

		/**
		* Sets up the styling and loads it for the current widget.
		*/
		$this->set_styling();

	    $settings = $this->get_settings();

	    global $duplicated_posts;
	    $duplicated_posts = array();

	    $args = array(
	        'post_type'         	=> 'post',
	        'posts_per_page'    		=> 3,
	        'order'             		=> $settings['order'],
	        'orderby'           		=> $settings['orderby']
	    );

	    $query = new \WP_Query( $args );

	    if ( $query->have_posts() ) : ?>
	        <div class="inline-posts-layout-1">
				<ul class="inline-posts--the-posts col-md-12">
				<?php
				while ( $query->have_posts() ) : $query->the_post(); ?> 
					<?php $duplicated_posts[] = get_the_ID(); ?>
					<li class="inline-posts--individual-post col-md-4">
						<?php if ( has_post_thumbnail() ) { ?>
						<div class="inline-posts--individual-post-image">
							<a class="img-href" href="" target="_blank">
								<?php the_post_thumbnail(); ?>
							</a>
						</div>
						<?php } ?> 
						<h3 class="inline-posts--individual-post-title">
							<a href="<?php the_permalink(); ?>" target="_blank"><?php get_the_title() ? the_title() : the_ID(); ?></a>
						</h3>
						<?php echo Helpers\_s_show_post_info( array( 'time' ) ); ?>
					</li>
				<?php
				endwhile;
				?>
				</ul>
	        </div>

	        <?php
	        wp_reset_postdata();
	    endif;
	}
	protected function content_template() {}

	public function render_plain_content() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Three_Inline_Posts() );