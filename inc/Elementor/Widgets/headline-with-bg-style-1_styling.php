<?php
?>
<style>
.big-color-block {
    background: -webkit-linear-gradient(left, #ffffff 20px, transparent 1%) center, -webkit-linear-gradient(#ffffff 20px, transparent 1%) center, rgba(0,0,0,0.1);
    background: linear-gradient(90deg, #ffffff 20px, transparent 1%) center, linear-gradient(#ffffff 20px, transparent 1%) center, rgba(0,0,0,0.1);
}
.big-color-block {
    background-size: 22px 22px;
    padding: 6% 3% 6% 3%;
}

.big-color-block .inner-block {
    text-align: center;
}

.big-color-block .inner-block .secondary-text {
    font-family: Helvetica, sans-serif;
    font-size: 14px;
    text-transform: uppercase;
    letter-spacing: 2px;
    color: var(--titleGray);
    font-weight: 600;
    opacity: 0.3;
}

.big-color-block .inner-block .main-text {
    color: var(--titleGray);
    opacity: 1;
    font-size: 36px;
    font-family: "Simoncini Garamond Italic", serif;
}
</style>