<?php
?>
<style>
.two-tall-posts-layout-1 {
    display: flex;
}

.two-tall-posts-layout-1--the-posts {
    justify-content: space-between;
    display: inline-flex;
    list-style: none;
}

.two-tall-posts-layout-1--individual-post {
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
}

.two-tall-posts-layout-1--individual-post .two-tall-posts-layout-1--individual-post-image {
    width: 100%;
    height: 700px;
}

.two-tall-posts-layout-1--individual-post-image img {
    display: block;
    object-fit: cover;
    height: 100%;
    width: 100%;
}

.two-tall-posts-layout-1--info {
    will-change: transform;
    position: absolute;
    bottom: -24px;
    background: white;
    padding: 24px;
    left: 50%;
    transform: translate3d(-50%,0,0);
    transition: transform 0.25s ease;
}

.two-tall-posts-layout-1--individual-post h3 {
    font-family:"Simoncini Garamond Italic";
    font-size: 30px;
    margin-bottom: 6px;
    text-align: center;
}

.two-tall-posts-layout-1--individual-post:hover .two-tall-posts-layout-1--info {
    transform: translate3d(-50%,-10%,0);
}

.two-tall-posts-layout-1--info {
    display: flex;
    flex-direction: column;
    align-items: center;
}

.two-tall-posts-layout-1--info .post-meta-info {
    text-align: center;
}

.two-tall-posts-layout-1--individual-post .posted-on {
    text-transform: uppercase;
    letter-spacing: 2px;
    font-size: 9px;
    font-family: 'Helvetica', sans-serif;
    font-weight: 800;
    opacity: 0.5;
}
</style>