<?php
namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Two_Tall_Posts extends Widget_Base {

	public function get_name() {
		return 'two-tall-posts';
	}

	public function get_title() {
		return __( '2 Tall Posts', 'elementor' );
	}

	public function get_icon() {
		return 'favorite';
	}

	public function set_styling() {
		$current_file = basename(__FILE__, '.php');
		$template_style = get_template_directory() . "/inc/Elementor/Widgets/{$current_file}_styling.php";

		if( file_exists($template_style ) ) {
			require_once $template_style;
		}
	}

	protected function _register_controls() {

	    $this->start_controls_section(
	        'two-tall-posts-section',
	        [
	            'label'         => __( '2 Tall Posts', 'elementor' ),
	        ]
	    );

	    $this->add_control(
	        'order',
	        [
	            'label'         => __( 'Order', 'elementor' ),
	            'type'          => Controls_Manager::SELECT,
	            'default'       => '',
	            'options'       => [
	                ''          => __( 'Default', 'elementor' ),
	                'DESC'      => __( 'DESC', 'elementor' ),
	                'ASC'       => __( 'ASC', 'elementor' ),
	            ],
	        ]
	    );

	    $this->add_control(
	        'orderby',
	        [
	            'label'         => __( 'Order By', 'elementor' ),
	            'type'          => Controls_Manager::SELECT,
	            'default'       => '',
	            'options'       => [
	                ''              => __( 'Default', 'elementor' ),
	                'date'          => __( 'Date', 'elementor' ),
	                'title'         => __( 'Title', 'elementor' ),
	                'name'          => __( 'Name', 'elementor' ),
	                'modified'      => __( 'Modified', 'elementor' ),
	                'author'        => __( 'Author', 'elementor' ),
	                'rand'          => __( 'Random', 'elementor' ),
	                'ID'            => __( 'ID', 'elementor' ),
	                'comment_count' => __( 'Comment Count', 'elementor' ),
	                'menu_order'    => __( 'Menu Order', 'elementor' ),
	            ],
	        ]
	    );

	    $this->end_controls_section();

	}
	protected function render() {

		/**
		* Sets up the styling and loads it for the current widget.
		*/
		$this->set_styling();

	    $settings = $this->get_settings();

	    global $duplicated_posts;

	    $args = array(
	        'post_type'         		=> 'post',
	        'posts_per_page'    		=> 2,
	        'order'             		=> $settings['order'],
	        'orderby'           		=> $settings['orderby'],
	        'post__not_in'     			=> $duplicated_posts
	    );

	    $query = new \WP_Query( $args );

	    if ( $query->have_posts() ) : ?>
	        <div class="two-tall-posts-layout-1">
				<ul class="two-tall-posts-layout-1--the-posts col-md-12">
				<?php
				while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php $duplicated_posts[] = get_the_ID(); ?>
					<li class="two-tall-posts-layout-1--individual-post col-md-6">
						<?php if ( has_post_thumbnail() ) { ?>
						<div class="two-tall-posts-layout-1--individual-post-image">
							<a class="img-href" href="" target="_blank">
								<?php the_post_thumbnail(); ?>
							</a>
						</div>
						<?php } ?>
						<div class="two-tall-posts-layout-1--info">
							<h3 class="two-tall-posts-layout-1--individual-post-title">
								<a href="<?php the_permalink(); ?>" target="_blank"><?php get_the_title() ? the_title() : the_ID(); ?></a>
							</h3>
							<?php 
							echo Helpers\_s_get_nice_post_time( array( 'additional-text' => 'on' ) ); 
							?>
						</div>
					</li>
				<?php
				endwhile;
				?>
				</ul>
	        </div>

	        <?php
	        wp_reset_postdata();
	    endif;
	}

	protected function content_template() {}

	public function render_plain_content() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Two_Tall_Posts() );