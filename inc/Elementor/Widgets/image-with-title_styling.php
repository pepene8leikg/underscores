<?php
?>
<style>
.big-image-box-with-title {
    display: flex;
    position: relative;
    height: 480px;
    width: 100%;
    margin-bottom: 72px;
}

.big-image-box-with-title .big-box {
	display: flex;
	flex-direction: column;
	align-items: center;
	background: rgba(255,255,255,1);
	padding: 48px;
	position: absolute;
    left: 40%;
    top: 50%;
    transform: translate3d(-50%,-5%,0);
}

.big-image-box-with-title .big-box .big-box--paragraph {
	text-align:center;
    font-family: Helvetica, sans-serif;
    font-size: 14px;
    text-transform: uppercase;
    letter-spacing: 2px;
    color: var(--titleGray);
    font-weight: 600;
    opacity: 0.3;
}

.big-image-box-with-title .big-box .bix-box--big-text {
	text-align: center;
    color: var(--titleGray);
    opacity: 1;
    font-size: 36px;
    font-family: "Simoncini Garamond Italic", serif;
}

.big-image-box-with-title .image {
    display: block;
    height: 100%;
    width: 100%;
    object-fit: cover;
}

</style>