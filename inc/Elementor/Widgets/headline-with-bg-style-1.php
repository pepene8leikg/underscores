<?php
namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Headline_Bg_Style_One extends Widget_Base {

	public function get_name() {
	    return 'headline-bg-style-1';
	}

	public function get_title() {
	    return __( 'Headline with Dotted BG', 'elementor' );
	}

	public function get_icon() {
	    return 'favorite';
	}

	public function set_styling() {
		$current_file = basename(__FILE__, '.php');
		$template_style = get_template_directory() . "/inc/Elementor/Widgets/{$current_file}_styling.php";

		if( file_exists($template_style ) ) {
			require_once $template_style;
		}
	}

	protected function _register_controls() {

	    $this->start_controls_section(
	        'headline-bg-style-1',
	        [
	            'label'         => __( 'Headline with Dotted Background', 'elementor' ),
	        ]
	    );

	    $this->add_control(
	        'paragraph',
	        [
	            'label'         => __( 'Paragraph', 'elementor' ),
	            'type'          => Controls_Manager::TEXT,
	            'default'       => 'welcome'
	        ]
	    );

	    $this->add_control(
	    	'title',
	    	[
	    		'label'			=> __( 'Title', 'elementor' ),
	    		'type'			=> Controls_Manager::TEXT,
	    		'default'		=> 'To My Fash Blog'
	    	]
	    );

	    $this->end_controls_section();

	}
	protected function render() {

		/**
		* Sets up the styling and loads it for the current widget.
		*/
		$this->set_styling();

	    $settings = $this->get_settings();
	    ?>

        <div class="category-block big-color-block">
	        <div class="inner-block">
	            <p class="secondary-text"><?php echo $settings['paragraph']; ?></p>
	            <p class="main-text"><?php echo $settings['title']; ?></p>
	        </div>
    	</div>

    <?php
	}

	protected function content_template() {}

	public function render_plain_content() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Headline_Bg_Style_One() );