<?php

class SproutLoader {

    private $dir;

    private $config = [
        'namespace'     => 'Sprout',
        'directory'     => 'includes',
        'file_pattern'  => '$1.php'
    ];

    public function __construct($dir) {
        $this->dir = $dir;
        spl_autoload_register([$this, 'spl_autoload_register']);
    }

    public function spl_autoload_register( $class_name ) {
        $class_name = str_replace(
            $this->config['namespace'],
            $this->config['directory'],
            $class_name
        );

        $class_name = str_replace( '\\', DIRECTORY_SEPARATOR, $class_name );

        $class_path = $this->dir . DIRECTORY_SEPARATOR . strtolower( $class_name );


        $class_path = preg_replace( '/([^\\\\]+$)/', $this->config['file_pattern'], $class_path );

        echo $class_path;
        echo "<br>";

        if( file_exists( $class_path ) ) {
            require_once $class_path;
        }
    }
}