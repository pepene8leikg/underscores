<?php

namespace Setup_Screen;

/**
 * Sets up the initial theme_mods.
 */
function setup_theme_mods() {
	$theme_mods = array( 
		'header_style' 					=> 'style-1',
		'header_custom_text_top' 		=> 'talking about',
		'header_custom_text_bottom' 	=> 'Fashion & Glam',
	);
	foreach( $theme_mods as $mod_name => $mod_value ) {
		set_theme_mod( $mod_name, $mod_value );
	}
}
//add_action( 'init', __NAMESPACE__ . '\\setup_theme_mods' );

// REMEMBER TO DELETE THIS SHIT ONCE THE INITIAL SETUP IS OVER.