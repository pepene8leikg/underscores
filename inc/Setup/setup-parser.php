<?php
/**
 *
 * The AJAX parser for our setup submits.
 *
 */
namespace Setup_Screen;

function parse_setup_data(){
	check_ajax_referer( 'ojwqriw431bwwey32ruewje', 'security' );
	$data = sanitize_text_field( $_POST['data'] );
	add_option( 'test_ajax_data', $data, '', 'yes' );
}
add_action ('wp_ajax_parse_setup_data', 'parse_setup_data');