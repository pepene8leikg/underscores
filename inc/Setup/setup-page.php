<?php

/**
 *
 * Setup Screen, For Initial Settings Setup
 * @package MoltenCore
 * @subpackage Setup_Screen
 *
 */
namespace Setup_Screen;

/** 
 *
 * Implements the Option Interface, deals with the get_option and its methods.
 * @param 		$option The option that will be checked.
 * @see 		https://developer.wordpress.org/reference/functions/get_option/ 
 * @since 		1.0
 *
 */
interface Option_Interface {
	static function add_the_option    ( $option );
	static function get_the_option    ( $option );
	static function remove_the_option ( $option );
	static function update_the_option ( $option, $updated_option_value );
}


/**
 * The Class that loads up the setup screen and renders it.
 *
 *
 */
final class Load_Setup_Screen implements Option_Interface {

	protected static $instance;

	private function __construct() {
	}

	// We load the necessary scripts & variables.
	public function load_required_scripts() {
		wp_enqueue_script( 'setup-theme', get_template_directory_uri() . '/js/setup_theme.js', array(), null );
		$params = array(
					  	'ajaxurl' 		=> admin_url('admin-ajax.php'),
					  	'ajax_nonce' 	=> wp_create_nonce('ojwqriw431bwwey32ruewje'),
					  	'ABSPATH'		=> get_template_directory_uri()
						);
		// Variables will be passed to JavaScript as an object 'passed_wp_variables'
		wp_localize_script( 'setup-theme', 'passed_wp_variables', $params );
	}

	/**
	 *
	 * As per the Option_Interface implement.
	 * @param 		$option The Option that we check / work with.
	 *
	 */
	static function add_the_option( $option = 'skipped_setup' ) {
		add_option( $option, False, 'no', 'yes' );
	}

	static function get_the_option( $option = 'skipped_setup' ) {
		get_option( $option );
	}

	static function remove_the_option( $option = 'skipped_setup' ) {
		delete_option( $option );
	}

	static function update_the_option( $option = 'skipped_setup', $updated_option_value ) {
		update_option( 'skipped_setup', $updated_option_value );
	}

	/**
	 *
	 * The function to render the screen. Provides initial HTML code that is complemented by
	 * the JS file.
	 * @see template_directory/js/setup_theme.js
	 *
	 */

	public function render_screen() {
		// Initial setup. If the option is not set, then do it.
		if( null == self::get_the_option() ) {
			add_option( $option = 'skipped_setup' );
			// No need to remove or add another one in order to avoid infinite setup loops.
		}
		if ( 'yes' == self::get_the_option() || !current_user_can( 'administrator' ) ) {
			return;
		} else {
			// If we are successful, let's go!
			add_action( 'wp_footer', array( $this, 'load_required_scripts' ) );
		}
		?>
		<div class="welcome_screen">
			<div class="top-part">
				<p class="welcome-text">Welcome!</p>
				<p class="more-text"> Shall We Set This Up?</p>
			</div>
			<div class="bottom-part">
				<div class="go_bg"></div>
				<div class="go"></div>
				<p class="go-text">Let's Do It!</p>
			</div>
		</div>
	<?php
	}

	public static function instance() {
		if( null == self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}
Load_Setup_Screen::instance();