<?php

namespace Social_Space\Interfaces\Post_Social_Meta;

/**
 * Social Links generator interface. Each class that implements this must follow the rule of 
 * creating the specific links for each network, then adding a wrapper and finally returning
 * the class itself. The class is built without a singleton, even if called plenty times because
 * re-instantion is needed for each post.
 * @see 		-
 * @since 		1.0.0
 */
interface Social_List {

	/**
	 * @since 		1.0.0
	 * @access 		public
	 */
    public function build_share_output();

	/**
	 * @since 		1.0.0
	 * @access 		public
	 */
    public function build_share_links();

	/**
	 * @since 		1.0.0
	 * @access 		public
	 */
    public function generate_share_links();
}