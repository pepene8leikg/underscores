<?php

namespace Social_Space;

/**
 * Interface to scrape most popular JSON-enabled social networks.
 */
interface Scrape_Interface {

	/* Builds the end-point, or the link that will be queried */
	public function build_endpoint();

	/* Ingests the pure, non-altered JSON data */
	public function get_json();

	/* Handles and cleans the data */
	public function get_data();
}

class Scrape_Instagram implements Scrape_Interface {

	protected $username;

	public function set_username( $username ) {
		$this->username = $username;
	}

	/**
	 * Builds the endpoint for the Instagram API, provided the username from constructor.
	 * @param $end_cursor	Used in case the user wants more pictures to be scrapped.
	 */
	public function build_endpoint() {
		return 'https://www.instagram.com/' . trim( strtolower( $this->username ) ) . '/?__a=1';
	}

	/**
	 * GETs the data from Instagram, provided the endpoint.
	 */
	public function get_json() {

		$transient_name = $this->username . '_instagram_data';

		if ( false === ( $data = get_transient( $transient_name ) ) )  {
			$request = wp_remote_get( $this->build_endpoint() );
			if( is_wp_error( $request ) ) {
				return new \WP_Error( 'site-down', esc_html__( 'Instagram may be down. Unable to communicate.', '_s') );
			}

			elseif( wp_remote_retrieve_response_code( $request ) !== 200 ) {
				return new \WP_Error( 'invalid-response', esc_html__( 'Got an invalid response.', '_s') );
			}

			else {
				set_transient( $transient_name, wp_remote_retrieve_body( $request ), 3600 );
			}
		}
		return $data;
	}

	/**
	 * Ingests the data from get_json() then converts / decodes into an array.
	 */
	public function get_data() {
   	 	$json = $this->get_json();

	    if( is_wp_error($json) ) {
	        return new \WP_Error( 'invalid-json', esc_html__( 'Something is wrong. Did you enter the right username?', '_s' ) );
	    } else {

			$data = json_decode( $json, true);

			if( !$data ) {
				return new \WP_Error( 'invalid-data', esc_html__( 'Something is wrong. Did you enter the right username?', '_s' ) );
			} else {
				return $data;
			}
		}
	}
}

class Generate_Instagram_Links {

	protected $instagram_object;
	protected $username;

	public function __construct( $username, Scrape_Instagram $instagram_object = null ) {
		$this->username = $username;
		$this->instagram_object = $instagram_object === null ? new Scrape_Instagram : $instagram_object;
		$this->instagram_object->set_username( $this->username );
	}

	/**
	 * Generates the link to the source image, as well as to the instagram post of each image
	 * requested by our script.
	 */
	protected function get_links() {
		$response = $this->instagram_object->get_data();
		/**
		 * Based on the array generated from get_data(), some nodes have resulted that contain    * information about
		 * each photo the user has, as such, we'll loop through each photo and access any data.
		 * @see ['user']['media']['nodes'] - individual node / image.
		 */
		if( is_wp_error($response) ) {
			return new \WP_Error( 'invalid-json', esc_html__( 'Ouch. The data was not parsed correctly. Cannot continue.' ) );
		} else {
			foreach( $response['user']['media']['nodes'] as $node ) {
				$image = array('real_link' => 'src' );
				$image['real_link'] = 'https://www.instagram.com/p/' . $node['code'] . '?taken-by=' . $this->username;
				$image['src'] = $node['thumbnail_resources'][0]['src'];
				$images_links[] = $image;
			}
		}
		return $images_links;
	}

	/**
	 * Getter for the instagram links (non-local).
	 */ 
	public function get_instagram_photos_links() {
		$links = $this->get_links();

		if( !$links ) {
			return new \WP_error( 'unknown-error', esc_html__( 'Something went wrong. Cannot get data. Check username or contact developer.') );
		} else {
			return $links;
		}
	}

	/**
	 * Gets the local photo links of the newly added instagram photo links, or retrieves the
	 * links if they already exist(to not flood the media library).
	 * @todo 		Add an "expiry" date for each image, or remove if no longer used.
	 */
	protected function get_local_links() {
		// Requires loading of the wp-admin scripts.
		require_once ('wp-load.php');
		require_once ('wp-admin/includes/admin.php');

		$urls = $this->get_instagram_photos_links();

		$local_images 	= array();

		foreach( $urls as $url ) {

			$tmp = download_url( $url['src'] );

			$file_array = array(
				'name' 		=> basename( $url['src'] ),
				'tmp_name'	 => $tmp
			);

			$wp_upload_dir = wp_get_upload_dir();
			$local_url = $wp_upload_dir['path'] . '/' . $file_array['name'];

			if( file_exists( $local_url ) ) {
				array_push( $local_images, ($wp_upload_dir['url'] . '/' . $file_array['name']) );
			} else {
				if ( is_wp_error( $tmp ) ) {
					@unlink( $file_array[ 'tmp_name' ] );
					return $tmp;
				}

				$GLOBALS['post'] = null;

				$post_id = '0';

				$id = media_handle_sideload( $file_array, $post_id, $desc = null, $post_data = array('post_content' => 'insta_image' ) );
				
				if ( is_wp_error( $id ) ) {
					@unlink( $file_array['tmp_name'] );
					return $id;
				}
				
				$value = wp_get_attachment_url( $id );

				array_push( $local_images, $value );
			}
		}
		return $local_images;
	}

	/**
	 * Getter for the instagram links (local).
	 */ 
	public function get_instagram_local_photos_links() {
		$links = $this->get_local_links();

		if( is_wp_error( $links) ) {
			return new \WP_error( 'unknown-error', esc_html__( 'I cannot get the local links of the pictures. Check with the developer.' ) );
		} else {
			return $links;
		}
	}
}