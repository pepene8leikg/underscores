<?php

namespace Social_Space;

/**
 *
 * Social Links generator interface. Each class that implements this must follow the rule of 
 * creating the specific links for each network, then adding a wrapper and finally returning
 * the class itself. The class is built without a singleton, even if called plenty times because
 * re-instantion is needed for each post.
 *
 */
interface Social_List {
    public function build_share_output();
    public function build_share_links();
    public function generate_share_links();
}


/**
 *
 * The main class that initializez the generation of the share links for each post share.
 * @param(s)           @see - _s_Post_Share_constructors.
 *
 */
class _s_Post_Share implements Social_List {

    /**
     * Consturctor for the Post_Share class.
     * @param $networks      What networks we want the post to be shared on.
     * @param $post          The post itself.
     * @param $style         Which style the sharer should have.
     * @see .share-style-2 in style.css to see what available styles we have.
     */
    public function __construct( $networks = array(), $post, $style ) {
        $this->networks         = $networks;
        $this->post             = $post;
        $this->style            = $style;
    }

   public function __toString() {
        $output = $this->build_share_links();

        return $output;
    }

    /**
     * Function to build the social share links. The way it works is we keep adding onto an
     * output based on user input.
     */
    public function build_share_output() {
        $output = '';

        foreach( $this->networks as $network ) {
            /** 
             * @see An if-if-if logic was chosen due to the fact that the script should continue  * to the next condition if the last is not met.
             */
            if( $network == 'facebook' ) {
                $output .= '<a class="facebook-share share-post-icon" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' . get_permalink() . '"><i class="sf-1"></i></a>';
            }

            if( $network == 'twitter' ) {
                $output .= '<a class="twitter-share share-post-icon" target="_blank" href="https://twitter.com/home?status=Check%20out%20this%20article:%20' . get_permalink() . '%20-%20' . '"><i class="sf-6"></i></a>';
            }

            if( $network == 'pinterest' ) {
                $image = wp_get_attachment_url( get_post_thumbnail_id( $this->post->ID ) );
                $output .= '<a class="pinterest-share share-post-icon" target="_blank" href="https://pinterest.com/pin/create/button/?url=' . get_permalink() . '&media=' . $image . '&description=' . get_the_title() . '"><i class="sf-5"></i></a>';
            }

            if( $network == 'gplus' ) {
                $output .= '<a class="gplus-share share-post-icon" target="_blank" href="https://plus.google.com/share?url=' . get_permalink() . '"><i class="sf-3"></i></a>';
            }
        }

        return $output;
    }

    /**
     * This function ultimately generates the sharer class itself, using data ingested from
     * generate_social_output.
     */
    public function build_share_links() {

        $networks_el = array_intersect( array( 'facebook', 'twitter', 'pinterest', 'gplus' ), $this->networks );

        if( empty( $networks_el ) ) {
            return new WP_Error( 'empty_networks', esc_html__( 'No networks were set up.', '_s') );
        } else {

            if ( is_single() ) {
                $output = '<div class="post-share ' . $this->style . '">' . '<div class="holder">';
                $output .= $this->build_share_output();
                $output .= '</div></div>';

                return $output;

            } else if ( !is_single() && $this->style == 'share-style-1' ) {
                /* If we're not on the post page and the style is '1', then we know that we should display the share buttons somewhere else, e.g: Homepage. */
                $output = '<div class="post-share share-style-1"><div class="holder">';
                $output .= $this->build_share_output();
                $output .= '</div></div>';

                return $output;
            } else {
                return '';
            }
        }
    }


    /**
     * The final function that (re)instantiates this class, based on the requirements of each call * made from outside to this class.
     */
    public function generate_share_links() {
        return new self( $this->networks, $this->post, $this->style );
    }
}