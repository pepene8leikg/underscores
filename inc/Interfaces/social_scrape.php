<?php

/**
 * Interface to scrape most popular JSON-enabled social networks.
 */

interface Scrape_Interface {

	/* Builds the end-point, or the link that will be queried */
	public function build_endpoint();

	/* Ingests the pure, non-altered JSON data */
	public function get_json();

	/* Handles and cleans the data */
	public function get_data();
}