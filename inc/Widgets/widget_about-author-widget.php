<?php
/**
 * Plugin Name: About Me Widget
 */

add_action( 'widget_init', 'custom_widget_about_author');
register_widget( 'custom_widget_about_author' );

class custom_widget_about_author extends Wp_Widget {

	/**
	 * Setup the Widget
	 */
	public function __construct() {

		/**
		 * Enqueues the scripts for the Customizer.
		 */
		add_action('admin_enqueue_scripts', array($this, 'scripts'));

		$widget_ops = array('classname' 	=> 'custom_widget_about_author',
							'description'	=> esc_html__('A widget to display information about an author.', '_s') 
							);

		$control_ops = array('id_base' 		=> 'custom_widget_about_author');

		parent::__construct( 'custom_widget_about_author', __('_s: About Me Widget', '_s_about_me_widget'), $widget_ops, $control_ops );
	}

	public function scripts() {
	   wp_enqueue_script( 'media-upload' );
	   wp_enqueue_media();
	   wp_enqueue_script( 'our_admin', get_template_directory_uri() . '/js/about_me_widget.js', array('jquery') );
	}

	public function widget( $args, $instance ) {
		extract( $args );
		$title 				= isset( $instance['title'] ) ? apply_filters('widget_title', $instance['title'] ) : '';
		$name 				= isset( $instance['name'] ) ? $instance['name'] : '';
		$image 				= isset( $instance['image'] ) ? $instance['image'] : '';
		$author_bio 		= isset( $instance['author_bio'] ) ? $instance['author_bio'] : '';
		$signature			= isset( $instance['signature'] ) ? $instance['signature'] : '';

		echo ent2ncr( $before_widget );

		if( $title ) {
			echo ent2ncr( $before_title . $title . $after_title );
		} ?>

		<div class="widget-about-author">
			<?php if( $image ): ?>
			<div class="author-image">
				<img src="<?php echo esc_url( $image );?>" alt="<?php echo esc_html( $name ); ?>"/>
			</div>
			<?php endif; ?>
			<?php if ( $name ): ?>
			<h5 class="author-heading"><?php echo $name; ?></h5>
			<?php endif; ?>
			<?php if( $author_bio ): ?>
			<p class="author-bio"><?php echo $author_bio; ?></p>
			<?php endif; ?>
			<?php if( $signature ) : ?>
			<div class="author-signature">
				<img src="<?php echo esc_url( $signature );?>" alt="<?php echo esc_html( $name ); ?>"/>
			</div>
			<?php endif; ?>
		</div>

		<?php
		echo ent2ncr( $after_widget );
	}

	function update( $new_instance, $old_instance ) {
		$instance['title']		= ( isset( $new_instance['title'] ) ) ? strip_tags($new_instance['title'] ) : '';
		$instance['name']		= ( isset( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
		$instance['image']		= ( isset( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
		$instance['author_bio']= ( isset( $new_instance['author_bio'] ) ) ? strip_tags( $new_instance['author_bio'] ) : '';
		$instance['signature']	= ( isset( $new_instance['signature'] ) ) ? strip_tags( $new_instance['signature'] ): '';

		return $instance;
	}

	function form( $instance ) {
		$defaults = array(  'title'			=> '',
							'name'			=> '',
							'image' 		=> '',
							'author_bio'	=> '',
							'signature'		=> ''
						);

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Form for Widget Title -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Widget Title:<strong>(Leave Blank to Hide)</strong></label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title'];?>" />
		</p>

		<!-- Form for Author Name -->
		<p>
			<label for="<?php echo $this->get_field_id( 'name' ); ?>">Name:</label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'name' ); ?>" name="<?php echo $this->get_field_name( 'name' ); ?>" value="<?php echo $instance['name'];?>" />
		</p>

		<!-- Form for Author's Image / Avatar -->
		<p>
			<label for="<?php echo $this->get_field_id( 'image' ); ?>">Image:</label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" value="<?php echo $instance['image'];?>" />
			<br>
			<button class="upload_image_button">Upload Image</button>
		</p>

		<!-- Form for Author's Description -->
		<p>
			<label for="<?php echo $this->get_field_id( 'author_bio' ); ?>">Description:</label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'author_bio' ); ?>" name="<?php echo $this->get_field_name( 'author_bio' ); ?>" value="<?php echo $instance['author_bio'];?>" />
			</textarea>
		</p>

		<!-- Form for Author's Signature -->
		<p>
			<label for="<?php echo $this->get_field_id( 'signature' ); ?>">Signature Image:</label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'signature' ); ?>" name="<?php echo $this->get_field_name( 'signature' ); ?>" value="<?php echo $instance['signature'];?>" />
			<br>
			<button class="upload_image_button">Upload Image</button>
		</p>

	<?php

	}
}