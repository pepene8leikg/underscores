<?php

function _s_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Main Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', '_s' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title title widget-title-style-12">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'			=> esc_html__( 'Footer Sidebar', '_s' ),
		'id'			=> 'sidebar-2',
		'description'	=> esc_html__( 'Add Widgets Here.', '_s' ),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s col-sm-12 col-xs-12">',
		'after_widget'	=> '</section>',
		'before_title'	=> '<h3 class="widget-title title widget-title-style-12">',
		'after_title'	=> '</h3>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );

function _s_setup_widget_notification() {
	require_once trailingslashit( THEME_DIR ) . 'inc/Widgets/Widgets_Helpers/notifications_error.php';
}

function _s_disregard_error__and_refresh( $callback = null ) {
	require_once trailingslashit( THEME_DIR ) . 'inc/Widgets/Widgets_Helpers/error_just-refresh.php';
	if( !is_null( $callback ) ) {
		call_user_func( $callback );
	}
}

/**
* Load Widgets
*/

$widgets = [
			'inc/Widgets/widget_about-author-widget.php',
			'inc/Widgets/widget_simple-image-widget.php',
			'inc/Widgets/widget_instagram-widget.php'
];

foreach( $widgets as $widget_path ) {
	include( trailingslashit( THEME_DIR ) . $widget_path );
}