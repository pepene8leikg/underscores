<style>
	.just-refresh {
		display: block;
		width: 100%;
	}
	.cta {
    	padding: 12px;
    	background: rgba(66, 244, 244,1);
	    font-size: 12px;
	    font-family: 'Helvetica', serif;
	    font-weight: 600;
	    text-transform: uppercase;
	    letter-spacing: 2px;
	    text-align: center;
	    border-radius: 3px;
	}
	.note {
		margin-bottom: 24px;
		text-align: center;
	}
</style>
<!-- REPLACE WITH HELPERS\STRINGS -->
<p class="note"><strong>Username is wrong</strong> or an unknown error occured. Refresh? </p>
<a class="just-refresh" href="<?php echo get_site_url(); ?>">
	<p class="cta">Refresh</p>
</a>