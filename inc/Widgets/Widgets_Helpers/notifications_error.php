<style>
	.setup-widget-notification {
		padding: 24px;
		background: #FF5F6D;
		background: -webkit-linear-gradient(to left, #FFC371, #FF5F6D);
		background: linear-gradient(to left, #FFC371, #FF5F6D);
		width: 100%;
		border-radius: 2px;
	}
	.setup-widget-notification p {
		color: white;
		text-align: center;
		font-size: 16px;
	}
}


</style>
<div class="setup-widget-notification">
	<p>Start Setting Up the Widget!</p>
</div>