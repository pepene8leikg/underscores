<?php
/**
 * Plugin Name: Simple Image Widget
 */

add_action( 'widget_init', 'custom_widget_simple_image');
register_widget( 'custom_widget_simple_image' );

class custom_widget_simple_image extends Wp_Widget {

	/**
	 * Setup the Widget
	 */
	public function __construct() {

		/**
		 * Enqueues the scripts for the Customizer
		 */
		add_action('admin_enqueue_scripts', array($this, 'scripts'));

		$widget_ops = array('classname' 	=> 'custom_widget_simple_image',
							'description'	=> esc_html__('A widget to display information about an author.', '_s') 
							);

		$control_ops = array('id_base' 		=> 'custom_widget_simple_image');

		parent::__construct( 'custom_widget_simple_image', __('_s: Simple Image Widget', '_s_simple_image_widget'), $widget_ops, $control_ops );
	}

	public function scripts() {
		/**
		 * These are the JS scripts used for the media uploader within the customizer.
		 */
	   wp_enqueue_script( 'media-upload' );
	   wp_enqueue_media();
	   wp_enqueue_script( 'our_admin', get_template_directory_uri() . '/js/about_me_widget.js', array('jquery') );
	}

	public function widget( $args, $instance ) {
		extract( $args );
		$title 				= isset( $instance['title'] ) ? apply_filters('widget_title', $instance['title'] ) : '';
		$link				= isset( $instance['link'] ) ? $instance['link'] : '';
		$image 				= isset( $instance['image'] ) ? $instance['image'] : '';

		echo ent2ncr( $before_widget );

		if ( $title ) {
			echo ent2ncr( $before_title . $title . $after_title );
		} ?>

		<div class="widget-image-1">
			<?php if( $image ): ?>
				<a href="<?php echo ( $link ? esc_url( $link ) : ''); ?>">
					<img src="<?php echo esc_url( $image ); ?>"/>
				</a>
			<?php endif; ?>
		</div>

		<?php
		echo ent2ncr( $after_widget );
	}

	function update( $new_instance, $old_instance ) {
		$instance['title']		= ( isset( $new_instance['title'] ) ) ? strip_tags($new_instance['title'] ) : '';
		$instance['link']		= ( isset( $new_instance['link'] ) ) ? strip_tags( $new_instance['link'] ) : '';
		$instance['image']		= ( isset( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';

		return $instance;
	}

	function form( $instance ) {
		$defaults = array(  'title'			=> '',
							'link'			=> '',
							'image' 		=> ''
						);

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<!-- Form for Title -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Widget Title:<strong>(Leave Blank to Hide)</strong></label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title'];?>" />
		</p>

		<!-- Form for Link -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>">Link:</label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo $instance['link'];?>" />
		</p>

		<!-- Form for Image -->
		<p>
			<label for="<?php echo $this->get_field_id( 'image' ); ?>">Image:</label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" value="<?php echo $instance['image'];?>" />
			<br>
			<button class="upload_image_button">Upload Image</button>
		</p>

	<?php
	}
}