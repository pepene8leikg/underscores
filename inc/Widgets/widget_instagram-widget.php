<?php
/**
 * Plugin Name: Instagram Widget
 */


add_action( 'widget_init', 'custom_widget_instagram_pictures');
register_widget( 'custom_widget_instagram_pictures' );

class custom_widget_instagram_pictures extends Wp_Widget {

	/**
	 * Setup the Widget
	 */
	public function __construct() {
		$widget_ops = array('classname' 	=> 'custom_widget_instagram_pictures',
							'description'	=> esc_html__('A widget to display instagram photos.', '_s') 
							);

		$control_ops = array('id_base' 		=> 'custom_widget_instagram_pictures');

		parent::__construct( 'custom_widget_instagram_pictures', __('_s: Instagram Widget', '_s_instagram_widget'), $widget_ops, $control_ops );
	}

	public function widget( $args, $instance ) {
		extract( $args );
		$title 				= isset( $instance['title'] ) ? apply_filters('widget_title', $instance['title'] ) : '';
		$username			= isset( $instance['username'] ) ? $instance['username'] : 'kinfolk';
		$profile_link		= isset( $instance['profile_link'] ) ? $instance['profile_link'] : '';
		$columns			= isset( $instance['columns'] ) ? $instance[ 'columns'] : '';
		$save_local 		= isset( $instance['save_local'] ) ? $instance['save_local'] : '';
		$number_of_photos	= isset( $instance['number_of_photos'] ) ? $instance['number_of_photos'] : '';

		echo ent2ncr( $before_widget );
		
		// Extremely problematic. Action cannot be unhooked this way.
		do_action('tooltips_instagram_widget', true);
		//Tooltip_Widget_Instagram::__hook_instagram_widget();

		if ( $title ) {
			echo ent2ncr( $before_title . $title . $after_title );
		}
		?>
		<div class="instagram-widget">
				<?php 
				if( $username ) {
					$instagram_object = new Social_Space\Generate_Instagram_Links( $username, new Social_Space\Scrape_Instagram );

					if ( !$save_local ) {

						$instagram_links = $instagram_object->get_instagram_photos_links();

						if( is_wp_error( $instagram_links ) ) {
							echo _s_disregard_error__and_refresh();
						} else {
							$instagram_links_parsed = 0;

							?>
							<ul class="instagram-widget-list">
							<?php
								foreach( $instagram_links as $link ) {
									if ( $instagram_links_parsed < absint( $number_of_photos ) ) {
										$instagram_links_parsed++;
										?>
										<li class="instagram-thumb instagram-thumb-<?php echo $columns ?>-col">
											<a href="<?php echo $link['real_link']; ?>"><img src="<?php echo $link['src']; ?>"/></a>
										</li>
										<?php
									}
									else {
										break;
									}
								} 
							?>
							</ul>
							<?php
						}
					}

					elseif ( $save_local ) {

						$instagram_links = $instagram_object->get_instagram_local_photos_links();

						if( is_wp_error( $instagram_links ) ) {
							echo "Ouch. Something's wrong. Did you enter the right username?";
							// Aici ar veni render_error().
						} else {
							$instagram_links_parsed = 0;

							?>

							<ul class="instagram-widget-list">
							<?php
								foreach( $instagram_links as $link ) {
									if ( $instagram_links_parsed < absint( $number_of_photos ) ) {
										$instagram_links_parsed++;
										?>
										<li class="instagram-thumb instagram-thumb-<?php echo $columns ?>-col">
											<a href="<?php echo $link; ?>"><img src="<?php echo $link; ?>"/></a>
										</li>
									<?php
									}
									else {
										break;
									}
								}
							?>
							</ul>
							<?php
						}
					}
				} else {
					echo _s_setup_widget_notification();
				} ?>
			<?php if( 'on' == $profile_link ) : ?>
			<button class="instagram-widget-profile-link"><a href="<?php echo 'https://www.instagram.com/' . $username; ?>"><i class="sf-4"></i><p><?php echo $username ?></p></a></button>
			<?php endif; ?>
		</div>
		<?php
		echo ent2ncr( $after_widget );
	}

	function update( $new_instance, $old_instance ) {
		$instance['title']			= ( isset( $new_instance['title'] ) ) ? strip_tags($new_instance['title'] ) : '';
		$instance['username']		= ( isset( $new_instance['username'] ) ) ? strip_tags( $new_instance['username'] ) : '';
		$instance['profile_link']	= ( isset( $new_instance['profile_link'] ) ) ? strip_tags( $new_instance['profile_link'] ) : '';
		$instance['columns']		= ( isset( $new_instance['columns'] ) ) ? strip_tags( $new_instance['columns'] ) : '';
		$instance['save_local']		= ( isset( $new_instance['save_local'] ) ) ? strip_tags( $new_instance['save_local'] ) : '';
		$instance['number_of_photos'] = ( isset( $new_instance['number_of_photos'] ) ) ? strip_tags( $new_instance['number_of_photos'] ) : '';

		if ( $new_instance['username'] !== $old_instance['username'] ) {
		        delete_transient( $old_instance['username'] . '_instagram_data' );
		}

		return $instance;
	}

	function form( $instance ) {
		$defaults = array(  'title'				=> '',
							'username'			=> 'kinfolk',
							'profile_link'		=> '',
							'columns'			=> '3',
							'save_local'		=> '',
							'number_of_photos' 	=> '12',
						);

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Custom Message -->
		<div class="customizer-custom-message-1">
			<p>Hey! Just one second. We strongly advise you keep an "even" number of images, 3 columns should have 3,6,9...photos. A 2 columns layout should have 2,4,6 and so on. Looks much better this way! </p>
		</div>
		<!-- Form for Title -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Widget Title:<strong>(Leave Blank to Hide)</strong></label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title'];?>" />
		</p>

		<!-- Form for Username -->
		<p>
			<label for="<?php echo $this->get_field_id( 'username' ); ?>">Username:</label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'username' ); ?>" name="<?php echo $this->get_field_name( 'username' ); ?>" value="<?php echo $instance['username'];?>" />
		</p>
		
		<!-- Checkbox for Local -->
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'save_local' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'save_local' ); ?>" name="<?php echo $this->get_field_name( 'save_local' ); ?>" /> 
    		<label for="<?php echo $this->get_field_id( 'save_local' ); ?>">Save Images to Local</label>
		</p>

		<!-- Checkbox for Profile Link -->
	    <p>
	        <input class="checkbox" type="checkbox" <?php checked( $instance[ 'profile_link' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'profile_link' ); ?>" name="<?php echo $this->get_field_name( 'profile_link' ); ?>" /> 
	        <label for="<?php echo $this->get_field_id( 'profile_link' ); ?>">Show Instagram Button?</label>
	    </p>

	    <!-- Columns -->
		<p>
			<label for="<?php echo $this->get_field_id( 'columns' ); ?>">How Many Columns to Display?<strong>(Leave Blank to Hide)</strong></label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'columns' ); ?>" name="<?php echo $this->get_field_name( 'columns' ); ?>" value="<?php echo $instance['columns'];?>" />
		</p>

	    <!-- Number of Photos -->
		<p>
			<label for="<?php echo $this->get_field_id( 'number_of_photos' ); ?>">How Many Pictures?</label>
			<br>
			<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_photos' ); ?>" name="<?php echo $this->get_field_name( 'number_of_photos' ); ?>" value="<?php echo $instance['number_of_photos'];?>" />
		</p>
	<?php
	}
}