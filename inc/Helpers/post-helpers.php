<?php
/**
 * Helpers that are related to the post.
 * @since       1.0.0
 * @package     _s
 * @subpackage  Helpers
 */
namespace Helpers;

/**
 * Gets the time of a post and wraps it in a nice output.
 * Must be called within a post page.
 * @param $args          Can call additional arguments dynamically.
 * @see                  "if ( isset( $args['additional-text'] ) ) {" within the function. Notice 
 *                       how we've used $args to re-build the output for the time.
 */
function _s_get_nice_post_time( $args = array() ) {
    $output = '';

    $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';

    $time_string = sprintf( $time_string,
        esc_attr( get_the_date( 'c' ) ),
        esc_html( get_the_date() ),
        esc_attr( get_the_modified_date( 'c' ) ),
        esc_html( get_the_modified_date() )
    );

    if ( isset( $args['additional-text'] ) ) {
        $posted_on = sprintf(
            /* translators: %s: post date. */
            esc_html_x($args['additional-text'] . ' %s','post date','_s'),
            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
            );

        $output .= '<span class="posted-on">' . $posted_on . '</span>';
    } else {
        $posted_on = sprintf(
            /* translators: %s: post date. */
            esc_html_x('%s','post date','_s'),
            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        );

        $output .= '<span class="posted-on">' . $posted_on . '</span>';
    }

    return $output;
}


/**
 * Gets the category of a post and wraps it in a nice output.
 * Must be called within a post page.
 * @param $args          Can call additional arguments dynamically.
 * @see                  "if ( isset( $args['additional-text'] ) ) {" within the function.
 *                       Notice how we've used $args to re-build the output for the time.
 */
function _s_get_nice_category( $args = array() ) {
    $output = '';

    $categories = (array) wp_get_post_terms( get_the_ID(),'category' );

    if ( isset( $args['additional-text'] ) ) {
        $category_string = sprintf(
            esc_html_x( $args['additional-text'] . ' %s','post category','_s'),
            '<a class="category">' . $categories[0]->name . '</a>'
        );
        $output .= '<span class="category">' . $category_string . '</span>';
    } else {
        $category_string = sprintf(
            esc_html_x('%s','post category','_s'),
            '<a class="category">' . $categories[0]->name . '</a>'
        );

        $output .= '<span class="category">' . $category_string . '</span>';
    }

    return $output;
}


/**
 * Gets the author of a post and wraps it in a nice output.
 * Must be called within a post page.
 * @param $args          Can call additional arguments dynamically.
 * @see                  "if ( isset( $args['additional-text'] ) ) {" within the function.
 *                       Notice how we've used $args to re-build the output for the time.
 */
function _s_get_nice_author( $args = array() ) {
    $output = '';

    if ( isset( $args['additional-text'] ) ) {
        $author = sprintf(
            /* translators: %s: post author. */
            esc_html_x( $args['additional-text'] . ' %s', 'post author', '_s' ),
            '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
        );

        $output .= '<span class="author"> ' . $author . '</span>';
    } else {
        $author = sprintf(
            /* translators: %s: post author. */
            esc_html_x( '%s', 'post author', '_s' ),
            '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
        );

        $output .= '<span class="author"> ' . $author . '</span>';
    }

    return $output;
}


/**
 * Gets the tags of a post and wraps it in a nice output.
 * Must be called within a post page.
 * @param $args          Can call additional arguments dynamically.
 * @see                  "if ( isset( $args['additional-text'] ) ) {" within the function.
 *                       Notice how we've used $args to re-build the output for the time.
 */
function _s_get_nice_tags( $post ) {
    $output         = '<ul class="post-tags">';
    $tags_list      = array();

    $tags = get_the_tags( $post->ID );

    if ( isset( $tags) ) {
        foreach( $tags as $tag ) {
            array_push( $tags_list, $tag->name );
        }
    }

    if ( isset( $args['additional-text'] ) ) {

        foreach( $tags_list as $tag ) {
            $tag_temp .= sprintf(
                /* translators: %s: post tag. */
                esc_html_x( $args['additional-text'] . '%s', 'post tags', '_s' ),
                $tag );

            $output .= '<li class="post-tag">' . $tag_temp . '</li>';
            unset( $tag_temp );
        }

    } else {
        foreach ( $tags_list as $tag ) {
            $tag_temp .= sprintf(
                /* translators: %s: post tag. */
                esc_html_x('%s', 'post tags', '_s' ),
                $tag );

            $output .= '<li class="post-tag">' . $tag_temp . '</li>';
            unset( $tag_temp );
        }
    }

    $output .= '</ul>';

    return $output;
}


/**
* Main function that ingests data from 'Helpers' and wraps it in the final container.
* @param $params        An array that is accessed to check for what data should be pulled from the
*                       Helpers class.
*/
function _s_show_post_info( $params = array() ){
    // array_intersect is used to set the order in place.
    $params = array_intersect ( array( 'time', 'author', 'category' ), $params ); 

    if ( empty ( $params ) ){
        $output = '<p class="post-has-no-info">No info about the post!';

    } else {
        $output = '';

        foreach ( $params as $param ) {
            if( $param == 'time' ) {
                $output .= _s_get_nice_post_time( $args = array( 'additional-text' => 'Posted on') );

            } elseif ( $param == 'author' ) {
                $output .= _s_get_nice_author( $args = array( 'additional-text' => 'by') );

            } else { /* Can only be the last remaining one, so, let's have a default! */
                $output .= _s_get_nice_category( $args = array( 'additional-text' => 'in') );
            }
        }
    }

    return '<div class="post-meta-info">' . $output . '</div>';
}


/**
 * Trims a string based on input length.
 */
function _s_trim_characters_from_output ( $function, $length, $end_line ) {
    return mb_strimwidth( $function, 0, $length, $end_line );
}



/**
 * Limits the characters in a post title.
 */
function _s_trim_post_title ( $length = null, $delimiter = null ) {
    $title          = get_the_title();
    $trimmed_title  = mb_strimwidth( $title, 
                                     0, 
                                     $length === null ? BIG_INT : $length, 
                                     '' // Won't use, bugs out.
                                    );
    $url            = esc_url( get_permalink() );

    if( strlen( $title ) == strlen( $trimmed_title ) ) {
        $delimiter = '';
    }

    $delimiter = $delimiter === null ? '' : (string)$delimiter;

    $output = '<h2 class="post-title"><a href="' . $url . '" rel="bookmark">' . $trimmed_title . $delimiter . '</a></h2>';

    return $output;
}