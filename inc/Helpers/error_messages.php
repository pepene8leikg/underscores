<?php

namespace Helpers;

/**
 * Class to retrieve error messages and render them out. Context aware due to the key pairs.
 * @param 		$errors The errors array, used to retrieve errors from.
 * @param 		$handle The key for the error.
 * @param 		$message The message / value for the error.
 * @since 		1.0.0
 * @package 	_s
 * @subpackage 	Helpers
 */
class Show_Error_Message {

	/*
	 * 
	 */
	public $errors;

	public $handle;

	public $message;

	public function __construct() {
		// Consider adding it to a "strings constant big array";
		$error_strings = array(
			'instagram' => array
						(
						'error-username-1'	=> __( 'I encountered an error. Is the username correct?', '_s' ),
						'error-username-2'	=> __( 'I still cannot work properly. Try a refresh!', '_s' )
						),
			'header' 	=> array 
						(
						'error-header-1' => __( 'Header error!', '_s' ),
						)
		);

		$this->errors = $error_strings;
	}

	public function raise_error() {
		return new \WP_Error( $this->handle, esc_html( $this->message ) );
	}

	public function render_error() {
		$output  = '<div class="notification-error ' . $this->handle . '">';
		$output .= '<p class="notification-error-message ' . $this->handle . '">' . $this->message . '</p></div>';
		return $output;
	}

	public function display_error( $error_domain, $error_handle ) {
		foreach( $this->errors[$error_domain] as $handle => $message ) {
			if( $error_handle == $handle ) {
				$this->handle = $handle; $this->message = $message;
				echo $this->render_error();
			}
		}
	}
}