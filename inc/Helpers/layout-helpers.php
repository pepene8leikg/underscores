<?php
/**
 * Helpers that are related to the layout, header and anything in-between.
 * @since 		1.0.0
 * @package 	_s
 * @subpackage 	Helpers
 */
namespace Helpers;

/**
 * Loads up a header file to be used as the displayed header.
 * @see 		header_style in customizer_register-settings.php to see how the customizer
 *				value is set for this.
 */
function get_header_style() {
	$header_style = get_theme_mod( 'header_style' );
	require_once ( THEME_DIR . "/partials/Headers/header-{$header_style}.php" );
}