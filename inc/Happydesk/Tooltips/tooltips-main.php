<?php

interface Tooltip_Interface {
	function get_tooltip_id();
	function get_tooltip_title();
	function get_tooltip_body();
	function get_tooltip_parent();
}

abstract class Tooltip_Helpers {
	/**
	 * Returns the merged info of the tooltip as array.
	 */
	abstract function generate_tooltip_info();
}

class Tooltip_Base extends Tooltip_Helpers implements Tooltip_Interface {

	/**
	 * The tooltip's custom html ID.
	 */
	protected $tooltip_id;

	/**
	 * The tooltip's <h3> title tag.
	 */
	protected $tooltip_title;

	/**
	 * The tooltip's inner html.
	 */
	protected $tooltip_body;

	/**
	 * The tooltip's context or location. Every tooltip is attached to a "parent" element, we hold that 
	 * parent name here.
	 */
	public $tooltip_parent;

	/**
	 * The tooltip's hook name. Used for location targeting.
	 */
	public $tooltip_hook_name;


	public function get_tooltip_id() {
		return $this->tooltip_id;
	}

	public function get_tooltip_title() {
		return esc_html( $this->tooltip_title );
	}

	public function get_tooltip_body() {
		return esc_html( $this->tooltip_body );
	}

	public function get_tooltip_parent() {
		return $this->tooltip_parent;
	}

	public function generate_tooltip_info() {
		$tooltip_info = [
			'tooltip_id'			=> $this->tooltip_id,
			'tooltip_parent_class'	=> $this->tooltip_parent
		];

		return $tooltip_info;
	}

	public function get_html() {
		$output = '';
		$output .= "<div style='position:absolute; z-index:1;' id='{$this->tooltip_id}' class='tooltip-helper'>";
		$output .= "<div class='tooltip-body'>{$this->tooltip_body}</div>";
		$output .= "</div><!--end .tooltip-helper-->";
		return $output;
	}

	public function __construct() {
		$this->tooltip_id 		= $this->get_tooltip_id();
		$this->tooltip_title 	= $this->get_tooltip_title();
		$this->tooltip_body  	= $this->get_tooltip_body();
		$this->tooltip_parent 	= $this->get_tooltip_parent();
		$this->tooltip_hook_name= $this->get_tooltip_hook();
	}
}

class Tooltips_Loader {

	private $tooltip;

	/**
	 * Loads the related JavaScript scripts and passes variables to it.
	 */
	public function load_required_scripts() {

		$handle = 'helper-tooltips';
		$state 	= 'enqueued';

		if (wp_script_is( $handle, $state ) ) {
			return;
		} else {
			echo "Enqueued the script!";
			wp_enqueue_script( $handle, get_template_directory_uri() . '/js/tooltips.js', array(), null );
			$params = array(
						  	'ajaxurl' 		=> admin_url('admin-ajax.php'),
						  	'ajax_nonce' 	=> wp_create_nonce('ojwqriw431bwwey32ruewje'),
						  	'redirect_url'	=> home_url(),
						  	'show_state'	=> get_option('tooltips_data')
							);
			wp_localize_script( $handle, 'passed_wp_variables', $params );
		}
	}

	function parse_tooltips_data() {
		check_ajax_referer( 'ojwqriw431bwwey32ruewje', 'security' );
		$data = sanitize_text_field( $_POST['tooltip_data'] );
		update_option( 'tooltips_data', $data, 'yes' );
	}

	public function register_tooltip( Tooltip_Base $tooltip ) {
		if ( current_user_can( 'administrator' ) ) {
			$this->tooltip = $tooltip;
			if( get_option('tooltips_data') === 'show' ) {
				add_action( $tooltip->get_tooltip_hook(), array( $this, 'output_tooltip' ) );
			} else {
				return;
			}
		} else {
			return;
		}
	}

	public function output_tooltip() {
		echo $this->tooltip->get_html();
	}

	public function __construct() {
		add_action( 'wp_footer', array( $this, 'load_required_scripts' ) );
		add_action ('wp_ajax_parse_tooltips_data', array($this, 'parse_tooltips_data'));
	}
}

// Ignore below.
// Note 1: Bring the action hook within the Tooltip Object itself.
// Note 2: Split each Tooltip into a file, have to re-write the handler to be accessed from other files in the same namespace

class Tooltip_Widget_Instagram extends Tooltip_Base {

	public function get_tooltip_id() {
		return '#instagram-help';
	}

	public function get_tooltip_title() {
		return 'Instagram Help';
	}

	public function get_tooltip_body() {
		return 'Need help with the instagram widget?';
	}

	public function get_tooltip_parent() {
		return 'widget';
	}

	public function get_tooltip_hook() {
		return 'tooltip__custom_instagram_widget_hook';
	}

	public static function __hook_instagram_widget() {
		do_action('tooltip__custom_instagram_widget_hook');
	}
}

class Tooltip_Header_Options extends Tooltip_Base {

	public function get_tooltip_id() {
		return '#header-help';
	}

	public function get_tooltip_title() {
		return 'Instagram Help';
	}

	public function get_tooltip_body() {
		return 'Wanna see more headers?';
	}

	public function get_tooltip_parent() {
		return 'header';
	}

	public function get_tooltip_hook() {
		return 'tooltip__header_options_hook';
	}

	public static function __hook_header_options() {
		do_action( 'tooltip__header_options_hook' );
	}
}

(new Tooltips_Loader)->register_tooltip(new Tooltip_Widget_Instagram());
(new Tooltips_Loader)->register_tooltip(new Tooltip_Header_Options());