<?php

namespace Tooltips;

class Tooltips_Handler {
	/**
	 * Holds all the tooltips. To be an array.
	 */
	private $tooltip;

	public function __construct( Tooltip_Base $tooltip ) {
		// Obviously, it must be able to register multiple tooltips at once.
		$this->tooltip = $tooltip;
	}

	public function output_tooltip() {
		echo $this->tooltip->get_html();
	}
}

?>