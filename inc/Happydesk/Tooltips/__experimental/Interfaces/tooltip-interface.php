<?php

namespace Tooltips\Interaces;

interface Tooltip_Interface {
	function get_tooltip_title();
	function get_tooltip_body();
	function get_tooltip_parent();
}

?>