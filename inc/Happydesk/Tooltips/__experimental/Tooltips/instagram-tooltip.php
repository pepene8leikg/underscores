<?php

//namespace?

class Tooltip_Widget_Instagram extends Tooltip_Base {

	public function __construct( $title, $body, $parent ) {
		parent::__construct( $title, $body, $parent );
	}
}

new Tooltips_Handler( new Tooltip_Widget_Instagram( 
	$title = 'Need Help With This Widget?', 
	$body = 'Check out or documentation!', 
	$parent = '.instagram-widget'
	) 
);

//$handler->output_tooltip();
?>