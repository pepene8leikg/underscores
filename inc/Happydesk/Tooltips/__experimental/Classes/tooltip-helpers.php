<?php

namespace Tooltips\Classes;

abstract class Tooltip_Helpers {
	/**
	 * Returns the merged info of the tooltip as array.
	 */
	abstract function generate_tooltip_info();
}
