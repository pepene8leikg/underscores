<?php

namespace Tooltips;

class Tooltip_Base extends Tooltip_Helpers implements Tooltip_Interface {

	/**
	 * The tooltip's <h3> title tag.
	 */
	protected $tooltip_title;


	/**
	 * The tooltip's inner html.
	 */
	protected $tooltip_body;


	/**
	 * The tooltip's context or location. Every tooltip is attached to a "parent" element, we hold that 
	 * parent name here.
	 */
	public $tooltip_parent;

	public function get_tooltip_title() {
		return $this->tooltip_title;
	}


	// Must create a body builder. The body isn't always going to be just a string. Must also translate __() the strings in the body.
	public function get_tooltip_body() {
		return $this->tooltip_body;
	}

	public function get_tooltip_parent() {
		return $this->tooltip_parent;
	}

	public function generate_tooltip_info() {
		$tooltip_info = [
			'tooltip_class'			=> $this->tooltip_title,
			'tooltip_parent_class'	=> $this->tooltip_parent
		];

		return $tooltip_info;
	}

	public function get_html() {
		$output = '';
		$output .= "<div style='position:absolute; z-index:1;' class='tooltip-helper {$this->tooltip_title}'>";
		$output .= "<div class='tooltip-body'>{$this->tooltip_body}</div>";
		$output .= "</div><!--end .tooltip-helper-->";
		return $output;
	}


	public function __construct( $tooltip_title, $tooltip_body, $tooltip_parent ) {
		$this->tooltip_title 	= $tooltip_title;
		$this->tooltip_body  	= $tooltip_body;
		$this->tooltip_parent 	= $tooltip_parent;
	}
}