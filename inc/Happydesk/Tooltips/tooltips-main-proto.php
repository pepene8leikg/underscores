<?php

/**
 * Base tooltip Interface, the only method employed reffers to a getter of an attribute that each tooltip
 * will have 1 or more of.
 */
interface Tooltip_Interface {
    public function get_attribute( $attribute );
}

/**
 * The main class that processes data, merges it and outputs, this is merely the structural phase of our
 * tooltip, which can be defined as simply an array of strings that we latter use.
 */
class Tooltip implements Tooltip_Interface {

    protected $data;

    private $is_data_processed = false;

    public function __construct( array $data ) {
        $this->data = $data;
    }

    protected function process_data() {
        if ( $this->is_data_processed ) {
            return;
        }

        $this->is_data_processed = true;

        $this->data = array_merge([
            'hook_name' => '',
            'id'        => '', 
            'title'     => '', 
            'body'      => '', 
            'meta'      => ''
        ], $this->data);

        $this->data['hook_name']    = $this->data['hook_name'];
        $this->data['id']           = stripslashes($this->data['id']);
        $this->data['title']        = stripcslashes($this->data['title']);
        $this->data['body']         = stripslashes($this->data['body']);
        $this->data['meta']         = stripslashes($this->data['meta']);
    }

    public function get_attribute( $attribute ) {
        $this->process_data();
        return $this->data[$attribute];
    }

    public function get_data() {
        return $this->data;
    }

    public function get_processed_state() {
        return $this->is_data_processed;
    }
}


/**
 * Employs the methods to retrieve the Tooltip's Style or how it should look and the Tooltip's Data
 * or the tooltip itself.
 * @@problem: This doesn't make sense to retrieve the tooltip here. The "get_tooltip" method should be
 * employed within the tooltip itself. Must re-think.
 */ 
interface Tooltip_Style_Interface {
    public function get_markup();
    public function get_tooltip();
}

/**
 * Employs the Style Interface to form a markup that is connected to CSS to express how the
 * Tooltip looks in terms of markup.
 */
class Tooltip_Style_1 implements Tooltip_Style_Interface {

    protected $tooltip;

    public function __construct( Tooltip_Interface $tooltip ) {
        $this->tooltip = $tooltip;
    }

    public function get_markup( $should_output = null ) {
        $markup = sprintf(
            '<div class="helper-tooltip" id=%s><div class="pulsate-holder"><span class="pulsate"></span><span class="pulsate-bg"></span></div><div class="inner-tooltip"><div class="main-body"><h4 class="tooltip-title">%s</h4><p class="tooltip-details">%s</p></div></div></div><!--%s-->', 
          $this->tooltip->get_attribute('id'), 
          $this->tooltip->get_attribute('title'), 
          $this->tooltip->get_attribute('body'), 
          $this->tooltip->get_attribute('meta')
        );

        if( $should_output ) {
            echo $markup;
        }

        return $markup;
    }

    public function get_tooltip() {
        return $this->tooltip;
    }
}


/**
 * Extended Video Class of the Tooltip, this allows us to create unlimited types of tooltips.
 * @param       data -  the data that is being passed to the "process_data" of each tooltip.
 *                      note that each Tooltip_Style_Interface instance treats this data
 *                      differently.
 */
class Tooltip_Video extends Tooltip {
    public function __construct( array $data ) {
        parent::__construct( $data );
    }
}

/**
 * Extends the markup of the Base Styling to add a video to the tooltip.
 */
class Tooltip_Style_Video implements Tooltip_Style_Interface {

    protected $tooltip;

    public function __construct( Tooltip_Interface $tooltip ) {
        $this->tooltip = $tooltip;
    }

    public function get_markup( $should_output = null ) {
        $markup = sprintf(
          '<div class="helper-tooltip" id=%s><div class="pulsate-holder"><span class="pulsate"></span><span class="pulsate-bg"></span></div><div class="inner-tooltip"><div class="main-body"><h4 class="tooltip-title">%s</h4><p class="tooltip-details">%s<a href="%s" class="video">Check Documentation Video</a></p></div></div></div><!--%s-->', 
          $this->tooltip->get_attribute('id'), 
          $this->tooltip->get_attribute('title'), 
          $this->tooltip->get_attribute('body'), 
          $this->tooltip->get_attribute('video'), 
          $this->tooltip->get_attribute('meta')
        );

        if( $should_output ) {
            echo $markup;
        }

        return $markup;
    }

    public function get_tooltip() {
        return $this->tooltip;
    }
}


/**
 * Registers all of the tooltips and stores them into an array. Singleton pattern used for the shared
 * resource nature of the tooltips array.
 */
class Tooltip_Register {

    protected $tooltips = [];

    private static $instance;

    private function __construct(){
    }

    public static function get_instance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    /**
     * Registers an individual tooltip by adding it to the tooltips array.
     * @param       tooltip - Individual tooltip.
     * @see         [search] "How To Register A Tooltip"
     */ 
    public function register_tooltip( Tooltip_Style_Interface $tooltip ) {
        $this->tooltips[] = $tooltip;
    }

    public function get_tooltips() {
        return $this->tooltips;
    }
}


/**
 * Loads the (java)scripts necessary for extra-functionality and passes WP variables to them.
 */
final class Tooltip_Load_Scripts {

    public function register_helper_tooltip_scripts() {
        add_action( 'wp_footer', array( $this, 'load_required_scripts' ));
        add_action( 'wp_ajax_parse_tooltips_data', array( $this, 'parse_tooltips_data' ));
    }

    public function load_required_scripts() {
        $handle = 'helper-tooltips';
        $state = 'enqueued';

        if ( wp_script_is( $handle, $state ) ) {
            return;
        }

        wp_enqueue_script( $handle, get_template_directory_uri() . '/js/tooltips.js', array(), null );
        $params = array(
            'ajaxurl'       => admin_url( 'admin-ajax.php' ),
            'ajax_nonce'    => wp_create_nonce( 'ojwqriw431bwwey32ruewje' ),
            'redirect_url'  => home_url(),
            'show_state'    => get_option( 'tooltips_data' )
        );
        wp_localize_script( $handle, 'passed_wp_variables', $params );
    }

    public function parse_tooltips_data() {
        check_ajax_referer( 'ojwqriw431bwwey32ruewje', 'security' );
        $data = sanitize_text_field( $_POST['tooltip_data'] );
        update_option( 'tooltips_data', $data, 'yes' );
    }
}


/**
 * Handles each tooltip individually and completes tasks such as hooking each tooltip output to an action,
 * rendering that tooltip.
 * @param       tooltips_holder -       An array of all the tooltips that were registered using 'Tooltip
 *                                      Register'.
 * @param       is_render_finished -    State of the class, should only be true after all rendering /     *                                      hooking specific to each tooltip is done.
 */
final class Tooltip_Handler {

    private $is_render_finished = false;

    private $tooltips_holder;

    public function __construct() {
        $this->tooltips_holder = Tooltip_Register::get_instance()->get_tooltips();
        (new Tooltip_Load_Scripts)->register_helper_tooltip_scripts();
    }


    /**
     * Renders and Hooks each Individual Tooltip registered.
     */
    public function render_tooltips() {
        if ($this->is_render_finished || get_option( 'tooltips_data' ) === 'hide' ) {
            return;
        }

        foreach( $this->tooltips_holder as $tooltip ) {

            add_action(
                $tooltip->get_tooltip()->get_data()['hook_name'],
                array($tooltip, 'get_markup'),
                10,
                true
            );
        }

        $this->is_render_finished = true;
    }

    public function get_render_state() {
        return $this->is_render_finished;
    }
}


/**
 * Registers a single tooltip with a given style and type.
 * @see         [search] "How To Register A Tooltip"
 */
Tooltip_Register::get_instance()->register_tooltip(
new Tooltip_Style_Video(
    new Tooltip_Video([
        'hook_name' => 'tooltips_headers_tooltip',
        'id'        => 'header-help',
        'title'     => 'Need help with the Headers?',
        'body'      => 'See Articles on The Headers.',
        'video'     => 'https://www.youtube.com/watch?v=_Jc-Xcw1nJI',
        'meta'      => 'Last update: last year'
    ])
));


/**
 * Registers a single tooltip with a given style and type.
 * @see         [search] "How To Register A Tooltip"
 */
Tooltip_Register::get_instance()->register_tooltip(
new Tooltip_Style_1(
    new Tooltip([ 
        'hook_name' => 'tooltips_instagram_widget',
        'id'        => 'instagram-help',
        'title'     => 'Instagram Widget not working?',
        'body'      => 'See Our Widgets Videos',
        'meta'      => 'Last update: last year'
    ])
));

(new Tooltip_Handler)->render_tooltips();