<?php

class Tooltip_Header_Options extends Tooltip_Base {

	public function get_tooltip_id() {
		return '#header-help';
	}

	public function get_tooltip_title() {
		return 'Instagram Help';
	}

	public function get_tooltip_body() {
		return 'Wanna see more headers?';
	}

	public function get_tooltip_parent() {
		return 'header';
	}

	public function get_tooltip_hook() {
		return 'tooltip__header_options_hook';
	}
}
(new Tooltips_Loader)->register_tooltip(new Tooltip_Header_Options());