<?php

class Tooltip_Widget_Instagram extends Tooltip_Base {

	public function get_tooltip_id() {
		return '#instagram-help';
	}

	public function get_tooltip_title() {
		return 'Instagram Help';
	}

	public function get_tooltip_body() {
		return 'Need help with the instagram widget?';
	}

	public function get_tooltip_parent() {
		return 'widget';
	}

	public function get_tooltip_hook() {
		return 'tooltip__custom_instagram_widget_hook';
	}
}
(new Tooltips_Loader)->register_tooltip(new Tooltip_Widget_Instagram());