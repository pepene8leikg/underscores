<?php

final class Contact_Tooltip {

    public static function show_tooltip() {
        echo '
            <div class="contact-theme-authors">
                <div class="note">
                        Mail us!
                </div>
                <div class="gooey-circles">
                <div class="circle c1">
                </div>
                <div class="circle c2">
                    <span class="close"><img src="close.svg"></span>
                </div>
                </div>
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
            <defs>
              <filter id="fancy-goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
              </filter>
            </defs>
        </svg>
            </div>
        ';
    }
}

?>