<?php

/**
 * Custom Customizer CSS.
 */
function my_enqueue_customizer_stylesheet() {

	wp_register_style( 'custom-customizer-css', get_template_directory_uri() . '/css/customizer-custom.css', NULL, NULL, 'all' );
	wp_enqueue_style( 'custom-customizer-css' );

}
add_action( 'customize_controls_print_styles', 'my_enqueue_customizer_stylesheet' );