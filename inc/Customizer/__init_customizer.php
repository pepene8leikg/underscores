<?php

/**
* Load Customizer.
*/
include( THEME_DIR . '/inc/Customizer/customizer_register-settings.php' );
add_action( 'customize_register', 'ju_customize_register' );

include( THEME_DIR . '/inc/Customizer/customizer_render-styles.php');
add_action( 'wp_head', 'ju_head' );
