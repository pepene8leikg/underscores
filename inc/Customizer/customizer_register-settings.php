<?php

function ju_customize_register( $wp_customize ) {

	/*
	$wp_customize->add_setting( 'header_bg_color', array(
														'default'	=> '#ffffff',
														'transport'	=> 'refresh'
	) );

	$wp_customize->add_section( 'ju_color_theme_section', array(
																'title'		=> __( 'Custom Color', '_s'),
																'priority'	=> 30
	) );
	$wp_customize->add_control( 
		new WP_Customize_Color_Control( $wp_customize,
										'theme_colors', 
										array(
											'label'		=> __( 'Header Color', '_s'),
											'section'	=> 'ju_color_theme_section',
											'settings'	=> 'header_bg_color')
	) );

	*/

	$wp_customize->add_section( 'ju_header_settings', array(
																'title'		=> __('Header Settings', '_s'),
																'priority'	=> 31
	) );

	$wp_customize->add_setting( 'header_custom_text_top', array(
																'default'	=> 'talking about',
																'transport'	=> 'refresh'
	) );

	$wp_customize->add_control( 
		new WP_Customize_Control( $wp_customize,
										'header_text_1', 
										array(
											'label'		=> __( 'Header Text #1', '_s'),
											'section'	=> 'ju_header_settings',
											'settings'	=> 'header_custom_text_top',
											'type'		=> 'text',
										)
	) );

	$wp_customize->add_setting( 'header_custom_text_bottom', array(
																'default'	=> 'Fashion & Glam',
																'transport'	=> 'refresh'
	) );

	$wp_customize->add_control( 
		new WP_Customize_Control( $wp_customize,
										'header_text_2', 
										array(
											'label'		=> __( 'Header Text #2', '_s'),
											'section'	=> 'ju_header_settings',
											'settings'	=> 'header_custom_text_bottom',
											'type'		=> 'text',
										)
	) );


	/* Header Settings */

	$wp_customize->add_setting( 'header_style', array(
													'default'	=> 'style-1',
													'transport'	=> 'refresh'
	) );

	$wp_customize->add_control( 
		new WP_Customize_Control( $wp_customize,
										'header_selector', 
										array(
											'label'		=> __( 'Select the Header You Want', '_s'),
											'section'	=> 'ju_header_settings',
											'settings'	=> 'header_style',
											'type'		=> 'radio',
											'choices'	=> array(
															'style-1'	=> 'Style One Header',
															'style-2'	=> 'Style Two Header',
															'style-3'	=> 'Style Three Header',
															'style-4'	=> 'Style Four Header',
															'style-5'	=> 'Style Five Header'
											)
										)
	) );
}


function _s_default_font_controls( $options ) {
	// Remove the default controls from Easy Google Fonts
	unset( $options['tt_default_heading_1'] );
	unset( $options['tt_default_heading_2'] );
	unset( $options['tt_default_heading_3'] );
	unset( $options['tt_default_heading_4'] );
	unset( $options['tt_default_heading_5'] );
	unset( $options['tt_default_heading_6'] );
	unset( $options['tt_default_body'] );

	$options['post_title_non_single'] = array(
		'name'			=> 'post_title_non_single',
		'title'			=> 'Post Titles Outside of Single Post Pages (homepage, etc).',
		'properties'	=> array(
								'selector' => '.post-title a, .single-post-title, .related-posts .related-the-post a',
							)
	);

	$options['secondary_text_info'] = array(
		'name'			=> 'secondary_text_info',
		'title'			=> 'Secondary Text, Mostly Informational',
		'properties'	=> array(
								'selector' => '.post-meta-info .posted-on, .post-meta-info .author, .post-meta-info .author a, .button-continue-reading a, .widget-title, .header .menu .menu-item > a, .section-title .wrap, .related-posts .related-the-post .posted-on a, .comment .comment-time, .comment .comment-content .comment-reply-link, #js-cover-menu-dropdown > li a, .single-post-meta, .header .search-top .search-top-form input[type="text"]'
							)
	);

	$options['paragraph_font'] = array(
		'name'			=> 'paragraph_font',
		'title'			=> 'Paragraph Font & Others',
		'properties'	=> array(
								'selector' => '.single-post-content p, .post-content p, .post-secondary-meta, .post-secondary-meta a, .single-post-category-span, .single-post-category-span a, .header .informatory-header-text .big-text, .header .informatory-header-text .text'
							)
	);

	return $options;
}

add_filter('tt_font_get_option_parameters', '_s_default_font_controls');