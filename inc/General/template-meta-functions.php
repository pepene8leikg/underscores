<?php

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _s_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );

/**
* Filter the excerpt length to 20 words.
*
* @param int $length Excerpt length.
* @return int (Maybe) modified excerpt length.
*/
function _s_custom_excerpt( $length ) {
	return 40;
}
add_filter( 'excerpt_length', '_s_custom_excerpt', 999 );

/**
* Adds a custom gravatar, to be used when the gravatar search fails!
*/
function _s_custom_avatar ($avatar_defaults) {
    $myavatar = "https://s3.amazonaws.com/uifaces/faces/twitter/mlane/128.jpg";
    $avatar_defaults[$myavatar] = "Custom Avatar";
    return $avatar_defaults;
}
add_filter( 'avatar_defaults', '_s_custom_avatar' );

/*
* Add specific CSS class by filter
*/
add_filter( 'body_class', 'my_class_names' );
function my_class_names( $classes ) {
    // add 'class-name' to the $classes array
    $classes[] = 'preloader-visible';
    // return the $classes array
    return $classes;
}
