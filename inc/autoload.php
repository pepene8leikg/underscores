<?php

$requirees = [
			'custom-header.php',
			'template-functions.php',
			'theme-setup.php',
			'constants.php',

			// Helpers
			'Helpers/post-helpers.php',
			'Helpers/layout-helpers.php',
			'Helpers/error_messages.php',


			// Social Tools
			'Social/post-share.php',
			'Social/instagram-scrapper.php',
			'Social/Interfaces/social_scrape.php',


			// Alter Core
			'Vendor/pagination.php',


			//Elementor
			'Elementor/__init_elementor.php',

			//Customizer
			'Customizer/__init_customizer.php',
			'Customizer/customizer_custom-css.php',


			//Widgets
			'Widgets/__init_widgets.php',


			//Walkers
			'Walkers/comment-walker.php',

			//Scripts
			'Scripts/scripts-setup.php',


			//General
			'General/template-meta-functions.php',


			//Setup
			'Setup/setup-defaults.php',

			//Happy Customers, Good Customers
			//'Happydesk/Tooltips/tooltips-main.php',
			//'Happydesk/Tooltips/tooltips-main-proto.php',
			'Happydesk/Contact/contact-tooltip.php',

			//Test

];

foreach ( $requirees as $requiree ) {
	require_once trailingslashit( THEME_DIR ) . 'inc/' . $requiree;
}