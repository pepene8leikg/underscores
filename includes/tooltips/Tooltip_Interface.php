<?php

namespace Sprout\Tooltips;

/**
 * Base tooltip Interface, the only method employed reffers to a getter of an attribute that each tooltip
 * will have 1 or more of.
 */
interface Tooltip_Interface {
    public function get_attribute( $attribute );
}

?>