<?php

namespace Sprout\Tooltips;

/**
 * Handles each tooltip individually and completes tasks such as hooking each tooltip output to an action,
 * rendering that tooltip.
 * @param       tooltips_holder -       An array of all the tooltips that were registered using 'Tooltip
 *                                      Register'.
 * @param       is_render_finished -    State of the class, should only be true after all rendering /     *                                      hooking specific to each tooltip is done.
 */
final class Tooltip_Handler {

    private $is_render_finished = false;

    private $tooltips_holder;

    public function __construct() {
        $this->tooltips_holder = Tooltip_Register::get_instance()->get_tooltips();
        (new Tooltip_Load_Scripts)->register_helper_tooltip_scripts();
    }


    /**
     * Renders and Hooks each Individual Tooltip registered.
     */
    public function render_tooltips() {
        if ($this->is_render_finished || get_option( 'tooltips_data' ) === 'hide' ) {
            return;
        }

        foreach( $this->tooltips_holder as $tooltip ) {

            add_action(
                $tooltip->get_tooltip()->get_data()['hook_name'],
                array($tooltip, 'get_markup'),
                10,
                true
            );
        }

        $this->is_render_finished = true;
    }

    public function get_render_state() {
        return $this->is_render_finished;
    }
}

(new Tooltip_Handler)->render_tooltips();

?>