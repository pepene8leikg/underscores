<?php

namespace Sprout\Tooltips\Tooltip_Style

/**
 * Extends the markup of the Base Styling to add a video to the tooltip.
 */
class Tooltip_Style_Video implements Tooltip_Style_Interface {

    protected $tooltip;

    public function __construct( Tooltip_Interface $tooltip ) {
        $this->tooltip = $tooltip;
    }

    public function get_markup( $should_output = null ) {
        $markup = sprintf(
          '<div class="helper-tooltip" id=%s><div class="pulsate-holder"><span class="pulsate"></span><span class="pulsate-bg"></span></div><div class="inner-tooltip"><div class="main-body"><h4 class="tooltip-title">%s</h4><p class="tooltip-details">%s<a href="%s" class="video">Check Documentation Video</a></p></div></div></div><!--%s-->', 
          $this->tooltip->get_attribute('id'), 
          $this->tooltip->get_attribute('title'), 
          $this->tooltip->get_attribute('body'), 
          $this->tooltip->get_attribute('video'), 
          $this->tooltip->get_attribute('meta')
        );

        if( $should_output ) {
            echo $markup;
        }

        return $markup;
    }

    public function get_tooltip() {
        return $this->tooltip;
    }
}

?>