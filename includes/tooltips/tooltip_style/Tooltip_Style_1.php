<?php


namespace Sprout\Tooltips\Tooltip_Style;

/**
 * Employs the Style Interface to form a markup that is connected to CSS to express how the
 * Tooltip looks in terms of markup.
 */
class Tooltip_Style_1 implements Tooltip_Style_Interface {

    protected $tooltip;

    public function __construct( Tooltip_Interface $tooltip ) {
        $this->tooltip = $tooltip;
    }

    public function get_markup( $should_output = null ) {
        $markup = sprintf(
            '<div class="helper-tooltip" id=%s><div class="pulsate-holder"><span class="pulsate"></span><span class="pulsate-bg"></span></div><div class="inner-tooltip"><div class="main-body"><h4 class="tooltip-title">%s</h4><p class="tooltip-details">%s</p></div></div></div><!--%s-->', 
          $this->tooltip->get_attribute('id'), 
          $this->tooltip->get_attribute('title'), 
          $this->tooltip->get_attribute('body'), 
          $this->tooltip->get_attribute('meta')
        );

        if( $should_output ) {
            echo $markup;
        }

        return $markup;
    }

    public function get_tooltip() {
        return $this->tooltip;
    }
}

?>