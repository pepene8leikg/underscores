<?php

namespace Sprout\Tooltips\Tooltip_Style\Interfaces;

/**
 * Employs the methods to retrieve the Tooltip's Style or how it should look and the Tooltip's Data
 * or the tooltip itself.
 * @@problem: This doesn't make sense to retrieve the tooltip here. The "get_tooltip" method should be
 * employed within the tooltip itself. Must re-think.
 */ 
interface Tooltip_Style_Interface {
    public function get_markup();
    public function get_tooltip();
}

?>