<?php

namespace Sprout\Tooltips;

/**
 * Loads the (java)scripts necessary for extra-functionality and passes WP variables to them.
 */
final class Tooltip_Load_Scripts {

    public function register_helper_tooltip_scripts() {
        add_action( 'wp_footer', array( $this, 'load_required_scripts' ));
        add_action( 'wp_ajax_parse_tooltips_data', array( $this, 'parse_tooltips_data' ));
    }

    public function load_required_scripts() {
        $handle = 'helper-tooltips';
        $state = 'enqueued';

        if ( wp_script_is( $handle, $state ) ) {
            return;
        }

        wp_enqueue_script( $handle, get_template_directory_uri() . '/js/tooltips.js', array(), null );
        $params = array(
            'ajaxurl'       => admin_url( 'admin-ajax.php' ),
            'ajax_nonce'    => wp_create_nonce( 'ojwqriw431bwwey32ruewje' ),
            'redirect_url'  => home_url(),
            'show_state'    => get_option( 'tooltips_data' )
        );
        wp_localize_script( $handle, 'passed_wp_variables', $params );
    }

    public function parse_tooltips_data() {
        check_ajax_referer( 'ojwqriw431bwwey32ruewje', 'security' );
        $data = sanitize_text_field( $_POST['tooltip_data'] );
        update_option( 'tooltips_data', $data, 'yes' );
    }
}

?>