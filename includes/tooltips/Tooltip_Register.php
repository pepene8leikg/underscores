<?php

namespace Sprout\Tooltips;

/**
 * Registers all of the tooltips and stores them into an array. Singleton pattern used for the shared
 * resource nature of the tooltips array.
 */
class Tooltip_Register {

    protected $tooltips = [];

    private static $instance;

    private function __construct(){
    }

    public static function get_instance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    /**
     * Registers an individual tooltip by adding it to the tooltips array.
     * @param       tooltip - Individual tooltip.
     * @see         [search] "How To Register A Tooltip"
     */ 
    public function register_tooltip( Tooltip_Style_Interface $tooltip ) {
        $this->tooltips[] = $tooltip;
    }

    public function get_tooltips() {
        return $this->tooltips;
    }
}

?>