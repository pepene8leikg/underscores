<?php

namespace Sprout\Tooltips;

/**
 * The main class that processes data, merges it and outputs, this is merely the structural phase of our
 * tooltip, which can be defined as simply an array of strings that we latter use.
 */
class Tooltip implements Tooltip_Interface {

    protected $data;

    private $is_data_processed = false;

    public function __construct( array $data ) {
        $this->data = $data;
    }

    protected function process_data() {
        if ( $this->is_data_processed ) {
            return;
        }

        $this->is_data_processed = true;

        $this->data = array_merge([
            'hook_name' => '',
            'id'        => '', 
            'title'     => '', 
            'body'      => '', 
            'meta'      => ''
        ], $this->data);

        $this->data['hook_name']    = $this->data['hook_name'];
        $this->data['id']           = stripslashes($this->data['id']);
        $this->data['title']        = stripcslashes($this->data['title']);
        $this->data['body']         = stripslashes($this->data['body']);
        $this->data['meta']         = stripslashes($this->data['meta']);
    }

    public function get_attribute( $attribute ) {
        $this->process_data();
        return $this->data[$attribute];
    }

    public function get_data() {
        return $this->data;
    }

    public function get_processed_state() {
        return $this->is_data_processed;
    }
}

?>