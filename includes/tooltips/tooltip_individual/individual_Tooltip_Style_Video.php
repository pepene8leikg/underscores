<?php

namespace Sprout\Tooltip\Tooltips_Individual;

/**
 * Registers a single tooltip with a given style and type.
 * @see         [search] "How To Register A Tooltip"
 */
Tooltip_Register::get_instance()->register_tooltip(
new Tooltip_Style_Video(
    new Tooltip_Video([
        'hook_name' => 'tooltips_headers_tooltip',
        'id'        => 'header-help',
        'title'     => 'Need help with the Headers?',
        'body'      => 'See Articles on The Headers.',
        'video'     => 'https://www.youtube.com/watch?v=_Jc-Xcw1nJI',
        'meta'      => 'Last update: last year'
    ])
));

?>