<?php

namespace Sprout\Tooltip\Tooltips_Individual;

/**
 * Registers a single tooltip with a given style and type.
 * @see         [search] "How To Register A Tooltip"
 */
Tooltip_Register::get_instance()->register_tooltip(
new Tooltip_Style_1(
    new Tooltip([ 
        'hook_name' => 'tooltips_instagram_widget',
        'id'        => 'instagram-help',
        'title'     => 'Instagram Widget not working?',
        'body'      => 'See Our Widgets Videos',
        'meta'      => 'Last update: last year'
    ])
));

?>