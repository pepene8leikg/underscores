<?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
	<div class="container">
		<section id="sidebar-footer" class="widget-area sidebar-secondary col-md-12 col-sm-12 col-xs-12" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
		</section><!-- #secondary-sidebar -->
	</div><!-- .sidebar-footer container -->
<?php } ?>