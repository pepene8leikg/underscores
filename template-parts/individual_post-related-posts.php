<?php
$tags = wp_get_post_tags( $post->ID );

if ( !empty( $tags ) ) {
    $first_tag = $tags[0]->term_id;
    $args = array(
                'tag__in'               => array( $first_tag ) ,
                'post__not_in'          => array( $post->ID ),
                'posts_per_page'        => 3,
                'ignore_sticky_posts'   => true
                );

    $my_query = new WP_Query($args); ?>

    <div class="related-posts col-md-12">
        <div class="comments-title section-title">
            <p class="wrap">Related Posts</p>
        </div>
        <ul class="related-the-posts-list">
            <?php if( $my_query->have_posts() ) {
                while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
                    <li class="related-the-post">
                        <?php if ( has_post_thumbnail() ) {
                            the_post_thumbnail();
                        } else { 
                            echo "Post has no thumb!"; 
                        } ?>
                        <h4 class="related-post-title">
                            <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo Helpers\_s_trim_post_title(18, '...');  ?>
                            </a>
                        </h4>
                        <p class="related-post-time">
                            <?php echo Helpers\_s_get_nice_post_time(); ?>
                        </p>
                    </li>
                <?php endwhile;
            } ?>
            <?php wp_reset_query(); ?>
        </ul>
    </div>
<?php
} else {
    ?>
    <div class="no-related-posts">
            <!-- add_translator -->
            <h3 class="no-related-title">No related posts found! </h3>
    </div>
    <?php
}